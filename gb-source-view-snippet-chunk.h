/* gb-source-view-snippet-chunk.h
 *
 * Copyright (C) 2013 Christian Hergert <christian@hergert.me>
 *
 * This file is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef GB_SOURCE_VIEW_SNIPPET_CHUNK_H
#define GB_SOURCE_VIEW_SNIPPET_CHUNK_H

#include <gtk/gtk.h>

G_BEGIN_DECLS

#define GB_TYPE_SOURCE_VIEW_SNIPPET_CHUNK            (gb_source_view_snippet_chunk_get_type())
#define GB_SOURCE_VIEW_SNIPPET_CHUNK(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj), GB_TYPE_SOURCE_VIEW_SNIPPET_CHUNK, GbSourceViewSnippetChunk))
#define GB_SOURCE_VIEW_SNIPPET_CHUNK_CONST(obj)      (G_TYPE_CHECK_INSTANCE_CAST ((obj), GB_TYPE_SOURCE_VIEW_SNIPPET_CHUNK, GbSourceViewSnippetChunk const))
#define GB_SOURCE_VIEW_SNIPPET_CHUNK_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass),  GB_TYPE_SOURCE_VIEW_SNIPPET_CHUNK, GbSourceViewSnippetChunkClass))
#define GB_IS_SOURCE_VIEW_SNIPPET_CHUNK(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), GB_TYPE_SOURCE_VIEW_SNIPPET_CHUNK))
#define GB_IS_SOURCE_VIEW_SNIPPET_CHUNK_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass),  GB_TYPE_SOURCE_VIEW_SNIPPET_CHUNK))
#define GB_SOURCE_VIEW_SNIPPET_CHUNK_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS ((obj),  GB_TYPE_SOURCE_VIEW_SNIPPET_CHUNK, GbSourceViewSnippetChunkClass))

typedef struct _GbSourceViewSnippetChunk        GbSourceViewSnippetChunk;
typedef struct _GbSourceViewSnippetChunkClass   GbSourceViewSnippetChunkClass;
typedef struct _GbSourceViewSnippetChunkPrivate GbSourceViewSnippetChunkPrivate;

struct _GbSourceViewSnippetChunk
{
   GObject parent;

   /*< private >*/
   GbSourceViewSnippetChunkPrivate *priv;
};

struct _GbSourceViewSnippetChunkClass
{
   GObjectClass parent_class;
};

gboolean                  gb_source_view_snippet_chunk_contains         (GbSourceViewSnippetChunk *chunk,
                                                                         GtkTextBuffer            *buffer,
                                                                         GtkTextIter              *location);
void                      gb_source_view_snippet_chunk_draw             (GbSourceViewSnippetChunk *chunk,
                                                                         GtkWidget                *widget,
                                                                         cairo_t                  *cr);
void                      gb_source_view_snippet_chunk_build_marks      (GbSourceViewSnippetChunk *chunk,
                                                                         GtkTextBuffer            *buffer);
void                      gb_source_view_snippet_chunk_select           (GbSourceViewSnippetChunk *chunk);
GType                     gb_source_view_snippet_chunk_get_type         (void) G_GNUC_CONST;
gint                      gb_source_view_snippet_chunk_get_tab_stop     (GbSourceViewSnippetChunk *chunk);
gint                      gb_source_view_snippet_chunk_get_linked_chunk (GbSourceViewSnippetChunk *chunk);
gboolean                  gb_source_view_snippet_chunk_get_modified     (GbSourceViewSnippetChunk *chunk);
void                      gb_source_view_snippet_chunk_finish           (GbSourceViewSnippetChunk *chunk);
void                      gb_source_view_snippet_chunk_insert           (GbSourceViewSnippetChunk *chunk,
                                                                         GtkTextBuffer            *buffer,
                                                                         GtkTextIter              *location);
void                      gb_source_view_snippet_chunk_remove           (GbSourceViewSnippetChunk *chunk);
GbSourceViewSnippetChunk *gb_source_view_snippet_chunk_copy             (GbSourceViewSnippetChunk *chunk);
void                      gb_source_view_snippet_chunk_set_modified     (GbSourceViewSnippetChunk *chunk,
                                                                         gboolean                  modified);
void                      gb_source_view_snippet_chunk_update           (GbSourceViewSnippetChunk *chunk,
                                                                         GbSourceViewSnippetChunk *linked,
                                                                         GtkTextBuffer            *buffer);

G_END_DECLS

#endif /* GB_SOURCE_VIEW_SNIPPET_CHUNK_H */
