/* gb-source-gutter-renderer-changes.h
 *
 * Copyright (C) 2013 Christian Hergert <christian@hergert.me>
 *
 * This file is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef GB_SOURCE_GUTTER_RENDERER_CHANGES_H
#define GB_SOURCE_GUTTER_RENDERER_CHANGES_H

#include <gtksourceview/gtksourcegutterrenderer.h>

#include "gb-change-tracker.h"

G_BEGIN_DECLS

#define GB_TYPE_SOURCE_GUTTER_RENDERER_CHANGES            (gb_source_gutter_renderer_changes_get_type())
#define GB_SOURCE_GUTTER_RENDERER_CHANGES(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj), GB_TYPE_SOURCE_GUTTER_RENDERER_CHANGES, GbSourceGutterRendererChanges))
#define GB_SOURCE_GUTTER_RENDERER_CHANGES_CONST(obj)      (G_TYPE_CHECK_INSTANCE_CAST ((obj), GB_TYPE_SOURCE_GUTTER_RENDERER_CHANGES, GbSourceGutterRendererChanges const))
#define GB_SOURCE_GUTTER_RENDERER_CHANGES_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass),  GB_TYPE_SOURCE_GUTTER_RENDERER_CHANGES, GbSourceGutterRendererChangesClass))
#define GB_IS_SOURCE_GUTTER_RENDERER_CHANGES(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), GB_TYPE_SOURCE_GUTTER_RENDERER_CHANGES))
#define GB_IS_SOURCE_GUTTER_RENDERER_CHANGES_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass),  GB_TYPE_SOURCE_GUTTER_RENDERER_CHANGES))
#define GB_SOURCE_GUTTER_RENDERER_CHANGES_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS ((obj),  GB_TYPE_SOURCE_GUTTER_RENDERER_CHANGES, GbSourceGutterRendererChangesClass))

typedef struct _GbSourceGutterRendererChanges        GbSourceGutterRendererChanges;
typedef struct _GbSourceGutterRendererChangesClass   GbSourceGutterRendererChangesClass;
typedef struct _GbSourceGutterRendererChangesPrivate GbSourceGutterRendererChangesPrivate;

struct _GbSourceGutterRendererChanges
{
   GtkSourceGutterRenderer parent;

   /*< private >*/
   GbSourceGutterRendererChangesPrivate *priv;
};

struct _GbSourceGutterRendererChangesClass
{
   GtkSourceGutterRendererClass parent_class;
};

GType                    gb_source_gutter_renderer_changes_get_type (void) G_GNUC_CONST;
GtkSourceGutterRenderer *gb_source_gutter_renderer_changes_new      (GbChangeTracker *tracker);

G_END_DECLS

#endif /* GB_SOURCE_GUTTER_RENDERER_CHANGES_H */
