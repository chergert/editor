/* gb-source-view-snippet.c
 *
 * Copyright (C) 2013 Christian Hergert <christian@hergert.me>
 *
 * This file is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <glib/gi18n.h>

#include "gb-source-view-snippet.h"
#include "gb-source-view-snippet-chunk.h"

G_DEFINE_TYPE(GbSourceViewSnippet, gb_source_view_snippet, G_TYPE_OBJECT)

struct _GbSourceViewSnippetPrivate
{
   GPtrArray   *chunks;
   gchar       *key;
   GtkTextMark *mark_begin;
   GtkTextMark *mark_end;
   gint         tab_stop;
   gboolean     updating_chunks;
};

enum
{
   PROP_0,
   PROP_KEY,
   PROP_MARK_BEGIN,
   PROP_MARK_END,
   LAST_PROP
};

static GParamSpec *gParamSpecs[LAST_PROP];

GbSourceViewSnippet *
gb_source_view_snippet_copy (GbSourceViewSnippet *snippet)
{
   GbSourceViewSnippetPrivate *priv;
   GbSourceViewSnippetChunk *chunk;
   GbSourceViewSnippet *ret;
   guint i;

   g_return_val_if_fail(GB_IS_SOURCE_VIEW_SNIPPET(snippet), NULL);

   priv = snippet->priv;

   ret = g_object_new(GB_TYPE_SOURCE_VIEW_SNIPPET,
                      "key", priv->key,
                      NULL);
   for (i = 0; i < priv->chunks->len; i++) {
      chunk = g_ptr_array_index(priv->chunks, i);
      chunk = gb_source_view_snippet_chunk_copy(chunk);
      g_ptr_array_add(ret->priv->chunks, chunk);
   }

   return ret;
}

void
gb_source_view_snippet_add_chunk (GbSourceViewSnippet      *snippet,
                                  GbSourceViewSnippetChunk *chunk)
{
   g_return_if_fail(GB_IS_SOURCE_VIEW_SNIPPET(snippet));
   g_return_if_fail(GB_IS_SOURCE_VIEW_SNIPPET_CHUNK(chunk));
   g_ptr_array_add(snippet->priv->chunks, g_object_ref(chunk));
}

static GbSourceViewSnippetChunk *
gb_source_view_snippet_get_chunk_at_tab_stop (GbSourceViewSnippet *snippet,
                                              guint                tab_stop)
{
   GbSourceViewSnippetPrivate *priv;
   GbSourceViewSnippetChunk *chunk;
   guint i;

   g_return_val_if_fail(GB_IS_SOURCE_VIEW_SNIPPET(snippet), NULL);

   priv = snippet->priv;

   for (i = 0; i < priv->chunks->len; i++) {
      chunk = g_ptr_array_index(priv->chunks, i);
      if (gb_source_view_snippet_chunk_get_tab_stop(chunk) == tab_stop) {
         return chunk;
      }
   }

   return NULL;
}

static gint
get_max_tab_stop (GbSourceViewSnippet *snippet)
{
   GbSourceViewSnippetPrivate *priv;
   GbSourceViewSnippetChunk *chunk;
   gint max_tab_stop = -1;
   gint tab_stop;
   gint i;

   priv = snippet->priv;

   for (i = 0; i < priv->chunks->len; i++) {
      chunk = g_ptr_array_index(priv->chunks, i);
      tab_stop = gb_source_view_snippet_chunk_get_tab_stop(chunk);
      if (tab_stop > max_tab_stop) {
         max_tab_stop = tab_stop;
      }
   }

   return max_tab_stop;
}

gboolean
gb_source_view_snippet_move_next (GbSourceViewSnippet *snippet)
{
   GbSourceViewSnippetPrivate *priv;
   GbSourceViewSnippetChunk *chunk;
   GtkTextBuffer *buffer;
   GtkTextIter iter;
   gint max_tab_stop;

   g_return_val_if_fail(GB_IS_SOURCE_VIEW_SNIPPET(snippet), FALSE);

   priv = snippet->priv;

   max_tab_stop = get_max_tab_stop(snippet);

   if (++priv->tab_stop > max_tab_stop) {
      buffer = gtk_text_mark_get_buffer(priv->mark_begin);
      gtk_text_buffer_get_iter_at_mark(buffer, &iter, priv->mark_end);
      gtk_text_buffer_select_range(buffer, &iter, &iter);
      return FALSE;
   }

   if (!(chunk = gb_source_view_snippet_get_chunk_at_tab_stop(snippet, priv->tab_stop))) {
      return FALSE;
   }

   gb_source_view_snippet_chunk_select(chunk);

   return TRUE;
}

gboolean
gb_source_view_snippet_move_previous (GbSourceViewSnippet *snippet)
{
   GbSourceViewSnippetPrivate *priv;
   GbSourceViewSnippetChunk *chunk;
   GtkTextBuffer *buffer;
   GtkTextIter iter;

   g_return_val_if_fail(GB_IS_SOURCE_VIEW_SNIPPET(snippet), FALSE);

   priv = snippet->priv;

   if (--priv->tab_stop < 0) {
      buffer = gtk_text_mark_get_buffer(priv->mark_begin);
      gtk_text_buffer_get_iter_at_mark(buffer, &iter, priv->mark_begin);
      gtk_text_buffer_select_range(buffer, &iter, &iter);
      return FALSE;
   }

   if (!(chunk = gb_source_view_snippet_get_chunk_at_tab_stop(snippet, priv->tab_stop))) {
      return FALSE;
   }

   gb_source_view_snippet_chunk_select(chunk);

   return TRUE;
}

void
gb_source_view_snippet_insert (GbSourceViewSnippet *snippet,
                               GtkTextBuffer       *buffer,
                               GtkTextIter         *location)
{
   GbSourceViewSnippetPrivate *priv;
   GbSourceViewSnippetChunk *chunk;
   guint i;

   g_return_if_fail(GB_IS_SOURCE_VIEW_SNIPPET(snippet));
   g_return_if_fail(location);

   priv = snippet->priv;

   if (priv->mark_begin || priv->mark_end) {
      g_warning("Cannot overwrite previous snippet position.");
      return;
   }

   priv->mark_begin = gtk_text_buffer_create_mark(buffer, NULL, location, TRUE);

   for (i = 0; i < priv->chunks->len; i++) {
      chunk = g_ptr_array_index(priv->chunks, i);
      gb_source_view_snippet_chunk_insert(chunk, buffer, location);
   }

   priv->mark_end = gtk_text_buffer_create_mark(buffer, NULL, location, FALSE);

   for (i = 0; i < priv->chunks->len; i++) {
      chunk = g_ptr_array_index(priv->chunks, i);
      gb_source_view_snippet_chunk_build_marks(chunk, buffer);
   }

   g_object_notify_by_pspec(G_OBJECT(snippet), gParamSpecs[PROP_MARK_BEGIN]);
   g_object_notify_by_pspec(G_OBJECT(snippet), gParamSpecs[PROP_MARK_END]);
}

void
gb_source_view_snippet_finish (GbSourceViewSnippet *snippet)
{
   GbSourceViewSnippetPrivate *priv;
   GbSourceViewSnippetChunk *chunk;
   GtkTextBuffer *buffer;
   guint i;

   g_return_if_fail(GB_IS_SOURCE_VIEW_SNIPPET(snippet));

   priv = snippet->priv;

   if (!priv->mark_begin || !priv->mark_end) {
      g_warning("Cannot finish snippet as it has not been inserted.");
      return;
   }

   buffer = gtk_text_mark_get_buffer(priv->mark_begin);

   /*
    * TODO: Remove all our GtkTextTag in the range.
    */

   for (i = 0; i < priv->chunks->len; i++) {
      chunk = g_ptr_array_index(priv->chunks, i);
      gb_source_view_snippet_chunk_finish(chunk);
   }

   gtk_text_buffer_delete_mark(buffer, priv->mark_begin);
   gtk_text_buffer_delete_mark(buffer, priv->mark_end);
   g_clear_object(&priv->mark_begin);
   g_clear_object(&priv->mark_end);

   g_object_notify_by_pspec(G_OBJECT(snippet), gParamSpecs[PROP_MARK_BEGIN]);
   g_object_notify_by_pspec(G_OBJECT(snippet), gParamSpecs[PROP_MARK_END]);
}

void
gb_source_view_snippet_remove (GbSourceViewSnippet *snippet)
{
   GbSourceViewSnippetPrivate *priv;
   GbSourceViewSnippetChunk *chunk;
   GtkTextBuffer *buffer;
   GtkTextIter begin;
   GtkTextIter end;
   guint i;

   g_return_if_fail(GB_IS_SOURCE_VIEW_SNIPPET(snippet));

   priv = snippet->priv;

   if (!priv->mark_begin || !priv->mark_end) {
      g_warning("Cannot remove snippet as it has not been inserted.");
      return;
   }

   buffer = gtk_text_mark_get_buffer(priv->mark_begin);

   /*
    * TODO: Remove all our GtkTextTag in the range.
    */

   for (i = 0; i < priv->chunks->len; i++) {
      chunk = g_ptr_array_index(priv->chunks, i);
      gb_source_view_snippet_chunk_remove(chunk);
   }

   gtk_text_buffer_get_iter_at_mark(buffer, &begin, priv->mark_begin);
   gtk_text_buffer_get_iter_at_mark(buffer, &end, priv->mark_end);
   gtk_text_buffer_delete(buffer, &begin, &end);
   gtk_text_buffer_delete_mark(buffer, priv->mark_begin);
   gtk_text_buffer_delete_mark(buffer, priv->mark_end);
   g_clear_object(&priv->mark_begin);
   g_clear_object(&priv->mark_end);

   g_object_notify_by_pspec(G_OBJECT(snippet), gParamSpecs[PROP_MARK_BEGIN]);
   g_object_notify_by_pspec(G_OBJECT(snippet), gParamSpecs[PROP_MARK_END]);
}

void
gb_source_view_snippet_insert_text (GbSourceViewSnippet *snippet,
                                    GtkTextBuffer       *buffer,
                                    GtkTextIter         *location,
                                    const gchar         *text,
                                    guint                length)
{
   GbSourceViewSnippetPrivate *priv;
   GbSourceViewSnippetChunk *chunk;
   GbSourceViewSnippetChunk *linked;
   GtkTextMark *here;
   guint i;
   gint linked_to;

   g_return_if_fail(GB_IS_SOURCE_VIEW_SNIPPET(snippet));
   g_return_if_fail(GTK_IS_TEXT_BUFFER(buffer));
   g_return_if_fail(location);
   g_return_if_fail(text);

   priv = snippet->priv;

   if (priv->updating_chunks) {
      return;
   }

   priv->updating_chunks = TRUE;

   here = gtk_text_buffer_create_mark(buffer, NULL, location, TRUE);

   for (i = 0; i < priv->chunks->len; i++) {
      chunk = g_ptr_array_index(priv->chunks, i);
      if (gb_source_view_snippet_chunk_contains(chunk, buffer, location)) {
         gb_source_view_snippet_chunk_set_modified(chunk, TRUE);
      } else {
         linked_to = gb_source_view_snippet_chunk_get_linked_chunk(chunk);
         if (linked_to != -1) {
            linked = g_ptr_array_index(priv->chunks, linked_to);
            gb_source_view_snippet_chunk_update(chunk, linked, buffer);
            gtk_text_buffer_get_iter_at_mark(buffer, location, here);
         }
      }
   }

   gtk_text_buffer_get_iter_at_mark(buffer, location, here);
   gtk_text_buffer_delete_mark(buffer, here);

   priv->updating_chunks = FALSE;
}

void
gb_source_view_snippet_delete_range (GbSourceViewSnippet *snippet,
                                     GtkTextBuffer       *buffer,
                                     GtkTextIter         *begin,
                                     GtkTextIter         *end)
{
   g_print("Text deleted from snippet.\n");
}

void
gb_source_view_snippet_draw (GbSourceViewSnippet *snippet,
                             GtkWidget           *widget,
                             cairo_t             *cr)
{
   GbSourceViewSnippetPrivate *priv;
   GbSourceViewSnippetChunk *chunk;
   guint i;

   g_return_if_fail(GB_IS_SOURCE_VIEW_SNIPPET(snippet));

   priv = snippet->priv;

   for (i = 0; i < priv->chunks->len; i++) {
      chunk = g_ptr_array_index(priv->chunks, i);
      gb_source_view_snippet_chunk_draw(chunk, widget, cr);
   }
}

const gchar *
gb_source_view_snippet_get_key (GbSourceViewSnippet *snippet)
{
   g_return_val_if_fail(GB_IS_SOURCE_VIEW_SNIPPET(snippet), NULL);
   return snippet->priv->key;
}

void
gb_source_view_snippet_set_key (GbSourceViewSnippet *snippet,
                                const gchar         *key)
{
   g_return_if_fail(GB_IS_SOURCE_VIEW_SNIPPET(snippet));

   g_free(snippet->priv->key);
   snippet->priv->key = g_strdup(key);
   g_object_notify_by_pspec(G_OBJECT(snippet), gParamSpecs[PROP_KEY]);
}

GtkTextMark *
gb_source_view_snippet_get_mark_begin (GbSourceViewSnippet *snippet)
{
   g_return_val_if_fail(GB_IS_SOURCE_VIEW_SNIPPET(snippet), NULL);
   return snippet->priv->mark_begin;
}

GtkTextMark *
gb_source_view_snippet_get_mark_end (GbSourceViewSnippet *snippet)
{
   g_return_val_if_fail(GB_IS_SOURCE_VIEW_SNIPPET(snippet), NULL);
   return snippet->priv->mark_end;
}

static void
gb_source_view_snippet_finalize (GObject *object)
{
   GbSourceViewSnippetPrivate *priv;

   priv = GB_SOURCE_VIEW_SNIPPET(object)->priv;

   g_clear_pointer(&priv->key, g_free);

   G_OBJECT_CLASS(gb_source_view_snippet_parent_class)->finalize(object);
}

static void
gb_source_view_snippet_get_property (GObject    *object,
                                     guint       prop_id,
                                     GValue     *value,
                                     GParamSpec *pspec)
{
   GbSourceViewSnippet *snippet = GB_SOURCE_VIEW_SNIPPET(object);

   switch (prop_id) {
   case PROP_KEY:
      g_value_set_string(value, gb_source_view_snippet_get_key(snippet));
      break;
   case PROP_MARK_BEGIN:
      g_value_set_object(value, gb_source_view_snippet_get_mark_begin(snippet));
      break;
   case PROP_MARK_END:
      g_value_set_object(value, gb_source_view_snippet_get_mark_end(snippet));
      break;
   default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID(object, prop_id, pspec);
   }
}

static void
gb_source_view_snippet_set_property (GObject      *object,
                                     guint         prop_id,
                                     const GValue *value,
                                     GParamSpec   *pspec)
{
   GbSourceViewSnippet *snippet = GB_SOURCE_VIEW_SNIPPET(object);

   switch (prop_id) {
   case PROP_KEY:
      gb_source_view_snippet_set_key(snippet, g_value_get_string(value));
      break;
   default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID(object, prop_id, pspec);
   }
}

static void
gb_source_view_snippet_class_init (GbSourceViewSnippetClass *klass)
{
   GObjectClass *object_class;

   object_class = G_OBJECT_CLASS(klass);
   object_class->finalize = gb_source_view_snippet_finalize;
   object_class->get_property = gb_source_view_snippet_get_property;
   object_class->set_property = gb_source_view_snippet_set_property;
   g_type_class_add_private(object_class, sizeof(GbSourceViewSnippetPrivate));

   gParamSpecs[PROP_KEY] =
      g_param_spec_string("key",
                          _("Key"),
                          _("The key of the snippet."),
                          NULL,
                          G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS);
   g_object_class_install_property(object_class, PROP_KEY,
                                   gParamSpecs[PROP_KEY]);

   gParamSpecs[PROP_MARK_BEGIN] =
      g_param_spec_object("mark-begin",
                          _("Mark Begin"),
                          _("The beginning of the snippet region."),
                          GTK_TYPE_TEXT_MARK,
                          G_PARAM_READABLE | G_PARAM_STATIC_STRINGS);
   g_object_class_install_property(object_class, PROP_MARK_BEGIN,
                                   gParamSpecs[PROP_MARK_BEGIN]);

   gParamSpecs[PROP_MARK_END] =
      g_param_spec_object("mark-end",
                          _("Mark End"),
                          _("The endning of the snippet region."),
                          GTK_TYPE_TEXT_MARK,
                          G_PARAM_READABLE | G_PARAM_STATIC_STRINGS);
   g_object_class_install_property(object_class, PROP_MARK_END,
                                   gParamSpecs[PROP_MARK_END]);
}

static void
gb_source_view_snippet_init (GbSourceViewSnippet *snippet)
{
   snippet->priv = G_TYPE_INSTANCE_GET_PRIVATE(snippet,
                                               GB_TYPE_SOURCE_VIEW_SNIPPET,
                                               GbSourceViewSnippetPrivate);
   snippet->priv->tab_stop = -1;
   snippet->priv->chunks = g_ptr_array_new();
   g_ptr_array_set_free_func(snippet->priv->chunks,
                             (GDestroyNotify)g_ptr_array_unref);
}
