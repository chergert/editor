/* gb-change-tracker.h
 *
 * Copyright (C) 2013 Christian Hergert <christian@hergert.me>
 *
 * This file is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef GB_CHANGE_TRACKER_H
#define GB_CHANGE_TRACKER_H

#include <gtk/gtk.h>

G_BEGIN_DECLS

#define GB_TYPE_CHANGE_TRACKER            (gb_change_tracker_get_type())
#define GB_CHANGE_TRACKER(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj), GB_TYPE_CHANGE_TRACKER, GbChangeTracker))
#define GB_CHANGE_TRACKER_CONST(obj)      (G_TYPE_CHECK_INSTANCE_CAST ((obj), GB_TYPE_CHANGE_TRACKER, GbChangeTracker const))
#define GB_CHANGE_TRACKER_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass),  GB_TYPE_CHANGE_TRACKER, GbChangeTrackerClass))
#define GB_IS_CHANGE_TRACKER(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), GB_TYPE_CHANGE_TRACKER))
#define GB_IS_CHANGE_TRACKER_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass),  GB_TYPE_CHANGE_TRACKER))
#define GB_CHANGE_TRACKER_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS ((obj),  GB_TYPE_CHANGE_TRACKER, GbChangeTrackerClass))

typedef struct _GbChangeTracker        GbChangeTracker;
typedef struct _GbChangeTrackerClass   GbChangeTrackerClass;
typedef struct _GbChangeTrackerPrivate GbChangeTrackerPrivate;
typedef enum   _GbChangeTrackerState   GbChangeTrackerState;

enum _GbChangeTrackerState
{
   GB_CHANGE_TRACKER_NONE    = 0,
   GB_CHANGE_TRACKER_ADDED   = 1,
   GB_CHANGE_TRACKER_CHANGED = 2,
};

struct _GbChangeTracker
{
   GObject parent;

   /*< private >*/
   GbChangeTrackerPrivate *priv;
};

struct _GbChangeTrackerClass
{
   GObjectClass parent_class;
};

GType                 gb_change_tracker_get_type       (void) G_GNUC_CONST;
GbChangeTracker      *gb_change_tracker_new            (GtkTextBuffer   *buffer);
GbChangeTrackerState  gb_change_tracker_get_line_state (GbChangeTracker *tracker,
                                                        guint            line);
void                  gb_change_tracker_reset          (GbChangeTracker *tracker);

G_END_DECLS

#endif /* GB_CHANGE_TRACKER_H */
