/* gb-source-gutter-renderer-clang.c
 *
 * Copyright (C) 2013 Christian Hergert <christian@hergert.me>
 *
 * This file is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <glib/gi18n.h>

#include "gb-source-gutter-renderer-clang.h"

G_DEFINE_TYPE(GbSourceGutterRendererClang,
              gb_source_gutter_renderer_clang,
              GTK_SOURCE_TYPE_GUTTER_RENDERER)

static GdkPixbuf *gPixError;

struct _GbSourceGutterRendererClangPrivate
{
   GbClang *clang;
   gchar   *path;
};

enum
{
   PROP_0,
   PROP_CLANG,
   PROP_PATH,
   LAST_PROP
};

static GParamSpec *gParamSpecs[LAST_PROP];

GtkSourceGutterRenderer *
gb_source_gutter_renderer_clang_new (GbClang     *clang,
                                     const gchar *path)
{
   return g_object_new(GB_TYPE_SOURCE_GUTTER_RENDERER_CLANG,
                       "clang", clang,
                       "path", path,
                       NULL);
}

static void
gb_source_gutter_renderer_clang_begin (GtkSourceGutterRenderer *renderer,
                                       cairo_t                 *cr,
                                       GdkRectangle            *bg_area,
                                       GdkRectangle            *cell_area,
                                       GtkTextIter             *begin,
                                       GtkTextIter             *end)
{
}

static void
gb_source_gutter_renderer_clang_draw (GtkSourceGutterRenderer      *renderer,
                                      cairo_t                      *cr,
                                      GdkRectangle                 *bg_area,
                                      GdkRectangle                 *cell_area,
                                      GtkTextIter                  *begin,
                                      GtkTextIter                  *end,
                                      GtkSourceGutterRendererState  state)
{
   GbSourceGutterRendererClangPrivate *priv;
   GtkTextBuffer *buffer;

   priv = GB_SOURCE_GUTTER_RENDERER_CLANG(renderer)->priv;

   buffer = gtk_text_iter_get_buffer(begin);

   if (gb_clang_get_has_diagnostic(priv->clang, priv->path, buffer, begin, end)) {
      gdk_cairo_rectangle(cr, cell_area);
      gdk_cairo_set_source_pixbuf(cr, gPixError, cell_area->x, cell_area->y);
      cairo_fill(cr);
   }
}

static void
gb_source_gutter_renderer_clang_finalize (GObject *object)
{
   GbSourceGutterRendererClangPrivate *priv;

   priv = GB_SOURCE_GUTTER_RENDERER_CLANG(object)->priv;

   g_clear_object(&priv->clang);
   g_clear_pointer(&priv->path, g_free);

   G_OBJECT_CLASS(gb_source_gutter_renderer_clang_parent_class)->finalize(object);
}

static void
gb_source_gutter_renderer_clang_get_property (GObject    *object,
                                              guint       prop_id,
                                              GValue     *value,
                                              GParamSpec *pspec)
{
   GbSourceGutterRendererClang *clang = GB_SOURCE_GUTTER_RENDERER_CLANG(object);

   switch (prop_id) {
   case PROP_CLANG:
      g_value_set_object(value, clang->priv->clang);
      break;
   case PROP_PATH:
      g_value_set_string(value, clang->priv->path);
      break;
   default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID(object, prop_id, pspec);
   }
}

static void
gb_source_gutter_renderer_clang_set_property (GObject      *object,
                                              guint         prop_id,
                                              const GValue *value,
                                              GParamSpec   *pspec)
{
   GbSourceGutterRendererClang *clang = GB_SOURCE_GUTTER_RENDERER_CLANG(object);

   switch (prop_id) {
   case PROP_CLANG:
      clang->priv->clang = g_value_dup_object(value);
      break;
   case PROP_PATH:
      clang->priv->path = g_value_dup_string(value);
      break;
   default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID(object, prop_id, pspec);
   }
}

static void
gb_source_gutter_renderer_clang_class_init (GbSourceGutterRendererClangClass *klass)
{
   GObjectClass *object_class;
   GtkSourceGutterRendererClass *renderer_class;

   object_class = G_OBJECT_CLASS(klass);
   object_class->finalize = gb_source_gutter_renderer_clang_finalize;
   object_class->get_property = gb_source_gutter_renderer_clang_get_property;
   object_class->set_property = gb_source_gutter_renderer_clang_set_property;
   g_type_class_add_private(object_class, sizeof(GbSourceGutterRendererClangPrivate));

   renderer_class = GTK_SOURCE_GUTTER_RENDERER_CLASS(klass);
   renderer_class->begin = gb_source_gutter_renderer_clang_begin;
   renderer_class->draw = gb_source_gutter_renderer_clang_draw;

   gParamSpecs[PROP_CLANG] =
      g_param_spec_object("clang",
                          _("Clang"),
                          _("The GbClang instance."),
                          GB_TYPE_CLANG,
                          G_PARAM_READWRITE |
                          G_PARAM_STATIC_STRINGS |
                          G_PARAM_CONSTRUCT_ONLY);
   g_object_class_install_property(object_class, PROP_CLANG,
                                   gParamSpecs[PROP_CLANG]);

   gParamSpecs[PROP_PATH] =
      g_param_spec_string("path",
                          _("Path"),
                          _("The path to the target file."),
                          NULL,
                          G_PARAM_READWRITE |
                          G_PARAM_STATIC_STRINGS |
                          G_PARAM_CONSTRUCT_ONLY);
   g_object_class_install_property(object_class, PROP_PATH,
                                   gParamSpecs[PROP_PATH]);

   gPixError = gdk_pixbuf_new_from_file("error.png", NULL);
}

static void
gb_source_gutter_renderer_clang_init (GbSourceGutterRendererClang *clang)
{
   clang->priv = G_TYPE_INSTANCE_GET_PRIVATE(clang,
                                             GB_TYPE_SOURCE_GUTTER_RENDERER_CLANG,
                                             GbSourceGutterRendererClangPrivate);
}
