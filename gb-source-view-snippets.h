/* gb-source-view-snippets.h
 *
 * Copyright (C) 2013 Christian Hergert <christian@hergert.me>
 *
 * This file is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef GB_SOURCE_VIEW_SNIPPETS_H
#define GB_SOURCE_VIEW_SNIPPETS_H

#include "gb-source-view-snippet.h"

G_BEGIN_DECLS

#define GB_TYPE_SOURCE_VIEW_SNIPPETS            (gb_source_view_snippets_get_type())
#define GB_SOURCE_VIEW_SNIPPETS(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj), GB_TYPE_SOURCE_VIEW_SNIPPETS, GbSourceViewSnippets))
#define GB_SOURCE_VIEW_SNIPPETS_CONST(obj)      (G_TYPE_CHECK_INSTANCE_CAST ((obj), GB_TYPE_SOURCE_VIEW_SNIPPETS, GbSourceViewSnippets const))
#define GB_SOURCE_VIEW_SNIPPETS_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass),  GB_TYPE_SOURCE_VIEW_SNIPPETS, GbSourceViewSnippetsClass))
#define GB_IS_SOURCE_VIEW_SNIPPETS(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), GB_TYPE_SOURCE_VIEW_SNIPPETS))
#define GB_IS_SOURCE_VIEW_SNIPPETS_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass),  GB_TYPE_SOURCE_VIEW_SNIPPETS))
#define GB_SOURCE_VIEW_SNIPPETS_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS ((obj),  GB_TYPE_SOURCE_VIEW_SNIPPETS, GbSourceViewSnippetsClass))

typedef struct _GbSourceViewSnippets        GbSourceViewSnippets;
typedef struct _GbSourceViewSnippetsClass   GbSourceViewSnippetsClass;
typedef struct _GbSourceViewSnippetsPrivate GbSourceViewSnippetsPrivate;

struct _GbSourceViewSnippets
{
   GObject parent;

   /*< private >*/
   GbSourceViewSnippetsPrivate *priv;
};

struct _GbSourceViewSnippetsClass
{
   GObjectClass parent_class;
};

GType                 gb_source_view_snippets_get_type (void) G_GNUC_CONST;
void                  gb_source_view_snippets_add      (GbSourceViewSnippets *snippets,
                                                        GbSourceViewSnippet  *snippet);
GbSourceViewSnippet  *gb_source_view_snippets_lookup   (GbSourceViewSnippets *snippets,
                                                        const gchar          *key);
GbSourceViewSnippets *gb_source_view_snippets_new      (void);
void                  gb_source_view_snippets_remove   (GbSourceViewSnippets *snippets,
                                                        GbSourceViewSnippet  *snippet);

G_END_DECLS

#endif /* GB_SOURCE_VIEW_SNIPPETS_H */
