#include <gtk/gtk.h>
#include <gtksourceview/gtksource.h>

#include "gb-change-tracker.h"
#include "gb-clang.h"
#include "gb-search-overlay.h"
#include "gb-source-gutter-renderer-changes.h"
#include "gb-source-gutter-renderer-clang.h"
#include "gb-source-view.h"
#include "gb-source-view-snippet.h"
#include "gb-source-view-snippet-chunk.h"

static gboolean       gDark;
static GtkWidget     *search;
static GtkSourceView *sourceview;
static GtkWidget     *entry;
static GbClang       *clang;
static guint          translater;
static GtkWindow     *window;

static void
make_snippets (GbSourceView *sourceview)
{
   GbSourceViewSnippets *snippets;
   GbSourceViewSnippet *snippet;

   snippets = gb_source_view_get_snippets(sourceview);

   snippet = g_object_new(GB_TYPE_SOURCE_VIEW_SNIPPET,
                          "key", "pr",
                          NULL);

#define ADD_CHUNK(text, tab_stop, linked) \
G_STMT_START { \
   GbSourceViewSnippetChunk *chunk; \
   chunk = g_object_new(GB_TYPE_SOURCE_VIEW_SNIPPET_CHUNK, \
                        "text", text, \
                        "tab-stop", tab_stop, \
                        "linked-chunk", linked, \
                        NULL); \
   gb_source_view_snippet_add_chunk(snippet, chunk); \
   g_object_unref(chunk); \
} G_STMT_END

   ADD_CHUNK("printf (", -1, -1);
   ADD_CHUNK("\"", -1, -1);
   ADD_CHUNK("format", 0, -1);
   ADD_CHUNK("\"", -1, -1);
   ADD_CHUNK(", ", -1, -1);
   ADD_CHUNK("param", 1, -1);
   ADD_CHUNK(", ", -1, -1);
   ADD_CHUNK("param", 2, 5);
   ADD_CHUNK(", ", -1, -1);
   ADD_CHUNK("param", -1, 5);
   ADD_CHUNK(");", -1, -1);
   gb_source_view_snippets_add(snippets, snippet);

   snippet = g_object_new(GB_TYPE_SOURCE_VIEW_SNIPPET,
                          "key", "in",
                          NULL);
   ADD_CHUNK("#include \"", -1, -1);
   ADD_CHUNK("glib.h", 0, -1);
   ADD_CHUNK("\"", -1, -1);
   gb_source_view_snippets_add(snippets, snippet);

   snippet = g_object_new(GB_TYPE_SOURCE_VIEW_SNIPPET,
                          "key", "fail",
                          NULL);
   ADD_CHUNK("g_return_if_fail(", -1, -1);
   ADD_CHUNK("test", 0, -1);
   ADD_CHUNK(");", -1, -1);
   gb_source_view_snippets_add(snippets, snippet);

   snippet = g_object_new(GB_TYPE_SOURCE_VIEW_SNIPPET,
                          "key", "vfail",
                          NULL);
   ADD_CHUNK("g_return_val_if_fail(", -1, -1);
   ADD_CHUNK("test", 0, -1);
   ADD_CHUNK(", ", -1, -1);
   ADD_CHUNK("NULL", 1, -1);
   ADD_CHUNK(");", -1, -1);
   gb_source_view_snippets_add(snippets, snippet);
}

static gboolean
query_tooltip (GtkWidget  *widget,
               gint        x,
               gint        y,
               gboolean    keyboard_mode,
               GtkTooltip *tooltip,
               gpointer    user_data)
{
   GtkTextBuffer *buffer;
   GtkTextView *view = (GtkTextView *)widget;
   GtkTextIter iter;
   GtkTextIter end;
   gchar *text;

   gtk_text_view_window_to_buffer_coords(view, GTK_TEXT_WINDOW_WIDGET, x, y,
                                         &x, &y);
   gtk_text_view_get_iter_at_location(view, &iter, x, y);
   gtk_text_iter_set_line_offset(&iter, 0);
   gtk_text_iter_assign(&end, &iter);
   gtk_text_iter_forward_line(&end);
   buffer = gtk_text_view_get_buffer(view);
   text = gb_clang_get_tooltip(clang, "main.c", buffer, &iter, &end);
   gtk_tooltip_set_text(tooltip, text);
   g_free(text);

   return !!text;
}

static void
modified_changed (GtkTextBuffer *buffer,
                  gpointer       user_data)
{
   const gchar *title;

   if (window) {
      title = gtk_text_buffer_get_modified(buffer) ? "main.c *" : "main.c";
      gtk_window_set_title(window, title);
   }
}

static void
set_styling (GtkSourceView *sourceview,
             gboolean       dark)
{
   GtkSourceStyleSchemeManager *m;
   GtkSourceStyleScheme *s;
   GtkTextBuffer *b;
   GdkRGBA rgba;

   m = gtk_source_style_scheme_manager_get_default();
   s = gtk_source_style_scheme_manager_get_scheme(m, dark ? "solarizeddark"
                                                          : "solarized-light");
   b = gtk_text_view_get_buffer(GTK_TEXT_VIEW(sourceview));
   gtk_source_buffer_set_style_scheme(GTK_SOURCE_BUFFER(b), s);

   if (dark) {
      gdk_rgba_parse(&rgba, "#CCCCCC");
      rgba.alpha = 0.3;
   } else {
      //gdk_rgba_parse(&rgba, "#002b36");
      gdk_rgba_parse(&rgba, "#000000");
      rgba.alpha = 0.7;
   }

   g_object_set(search, "background-color", &rgba, NULL);
}

static void
set_language (GtkSourceView *sourceview)
{
   GtkSourceLanguageManager *m;
   GtkSourceLanguage *l;
   GtkTextBuffer *b;

   m = gtk_source_language_manager_get_default();
   l = gtk_source_language_manager_get_language(m, "c");
   b = gtk_text_view_get_buffer(GTK_TEXT_VIEW(sourceview));
   gtk_source_buffer_set_language(GTK_SOURCE_BUFFER(b), l);
}

static gboolean
window_key_press (GtkWindow     *window,
                  GdkEventKey   *event,
                  GtkSourceView *view)
{
   if (event->keyval == GDK_KEY_F11 && !event->state) {
      gDark = !gDark;
      set_styling(view, gDark);
      return TRUE;
   } else if (event->keyval == GDK_KEY_Escape && !event->state) {
      gtk_widget_grab_focus(GTK_WIDGET(sourceview));
      gtk_widget_hide(GTK_WIDGET(search));
      return TRUE;
   } else if (event->keyval == GDK_KEY_f && (event->state & GDK_CONTROL_MASK)) {
      gtk_widget_grab_focus(entry);
      return TRUE;
   }
   return FALSE;
}

static gboolean
do_translate (gpointer buffer)
{
   translater = 0;
   gb_clang_translate(clang, "main.c", buffer);
   gb_clang_highlight(clang, "main.c", buffer);
   return FALSE;
}

static void
queue_reparse (GtkTextBuffer *buffer,
               GbClang       *clang)
{
   if (translater) {
      g_source_remove(translater);
   }
   translater = g_timeout_add(1500, do_translate, buffer);
}

static void
entry_activate (GtkWidget *widget,
                gpointer   user_data)
{
   gboolean has_matches;

   g_object_get(sourceview,
                "has-matches", &has_matches,
                NULL);
   if (has_matches) {
      gb_source_view_move_next_match(GB_SOURCE_VIEW(sourceview));
      gtk_widget_show(GTK_WIDGET(search));
   }
}

static gchar **
pkgconfig_cflags (const gchar *pkgname,
                  ...)
{
   va_list args;
   GPtrArray *ptr;
   gchar **argv;
   gchar *standard_output = NULL;
   gchar *str;
   gint argc;

   va_start(args, pkgname);

   ptr = g_ptr_array_new();
   g_ptr_array_add(ptr, (gchar *)"pkg-config");
   g_ptr_array_add(ptr, (gchar *)"--cflags");
   g_ptr_array_add(ptr, (gchar *)pkgname);
   while ((str = va_arg(args, gchar *))) {
      g_ptr_array_add(ptr, str);
   }
   g_ptr_array_add(ptr, NULL);
   argv = (gchar **)g_ptr_array_free(ptr, FALSE);
   g_spawn_sync(".", argv, NULL, G_SPAWN_SEARCH_PATH, NULL, NULL,
                &standard_output, NULL, NULL, NULL);
   g_free(argv);
   g_shell_parse_argv(standard_output, &argc, &argv, NULL);
   g_free(standard_output);

   return argv;
}

gint
main (gint   argc,
      gchar *argv[])
{
   GtkSourceGutterRenderer *renderer;
   PangoFontDescription *font;
   GtkScrolledWindow *scroller;
   GtkSourceBuffer *buffer;
   GtkSourceGutter *gutter;
   GbChangeTracker *tracker;
   GtkTextIter iter;
   GtkOverlay *overlay;
   GtkWidget *vbox;
   gchar **args;
   gchar *contents;

   gtk_init(&argc, &argv);

   gtk_source_style_scheme_manager_prepend_search_path(
      gtk_source_style_scheme_manager_get_default(),
      ".");

   clang = gb_clang_new();
   buffer = gtk_source_buffer_new(NULL);
   sourceview = g_object_new(GB_TYPE_SOURCE_VIEW,
                             "buffer", buffer,
                             "show-line-numbers", TRUE,
                             "show-right-margin", TRUE,
                             "visible", TRUE,
                             NULL);

   gutter = gtk_source_view_get_gutter(sourceview, GTK_TEXT_WINDOW_LEFT);
   renderer = gb_source_gutter_renderer_clang_new(clang, "main.c");
   gtk_source_gutter_renderer_set_padding(renderer, 2, 1);
   gtk_source_gutter_renderer_set_size(renderer, 14);
   gtk_source_gutter_insert(gutter, renderer, -30);
   tracker = gb_change_tracker_new(GTK_TEXT_BUFFER(buffer));
   renderer = gb_source_gutter_renderer_changes_new(tracker);
   gtk_source_gutter_renderer_set_size(renderer, 1);
   gtk_source_gutter_renderer_set_padding(renderer, 2, 0);
   gtk_source_gutter_insert(gutter, renderer, -10);
   g_signal_connect(sourceview,
                    "query-tooltip",
                    G_CALLBACK(query_tooltip),
                    NULL);
   g_signal_connect(buffer,
                    "modified-changed",
                    G_CALLBACK(modified_changed),
                    NULL);
   g_file_get_contents("main.c", &contents, NULL, NULL);
   gtk_source_buffer_begin_not_undoable_action(buffer);
   gtk_text_buffer_set_text(GTK_TEXT_BUFFER(buffer), contents, -1);
   gtk_source_buffer_end_not_undoable_action(buffer);
   gtk_text_buffer_set_modified(GTK_TEXT_BUFFER(buffer), FALSE);
   gtk_text_buffer_get_start_iter(GTK_TEXT_BUFFER(buffer), &iter);
   gtk_text_buffer_select_range(GTK_TEXT_BUFFER(buffer), &iter, &iter);
   g_free(contents);
   g_signal_connect(buffer, "changed", G_CALLBACK(queue_reparse), clang);
   make_snippets(GB_SOURCE_VIEW(sourceview));
   set_language(sourceview);
   font = pango_font_description_from_string("Monospace 10");
   gtk_widget_override_font(GTK_WIDGET(sourceview), font);
   scroller = g_object_new(GTK_TYPE_SCROLLED_WINDOW,
                           "child", sourceview,
                           "visible", TRUE,
                           NULL);
   overlay = g_object_new(GTK_TYPE_OVERLAY,
                          "child", scroller,
                          "visible", TRUE,
                          NULL);
   search = g_object_new(GB_TYPE_SEARCH_OVERLAY,
                         "widget", sourceview,
                         "tag", "search-tag",
                         "visible", TRUE,
                         NULL);
   set_styling(sourceview, FALSE);
   gtk_overlay_add_overlay(overlay, search);
   args = pkgconfig_cflags("gtksourceview-3.0", "glib-2.0", "gtk+-3.0", NULL);
   gb_clang_add_buffer(clang, "main.c", GTK_TEXT_BUFFER(buffer), args);
   g_strfreev(args);
   entry = g_object_new(GTK_TYPE_ENTRY,
                        "has-frame", FALSE,
                        "visible", TRUE,
                        NULL);
   g_signal_connect(entry, "activate", G_CALLBACK(entry_activate), NULL);
   vbox = g_object_new(GTK_TYPE_VBOX,
                       "visible", TRUE,
                       NULL);
   g_object_bind_property(entry, "text", sourceview, "search-text",
                          G_BINDING_DEFAULT);
   g_object_bind_property(sourceview, "has-matches", search, "visible",
                          G_BINDING_SYNC_CREATE);
   gtk_container_add(GTK_CONTAINER(vbox), GTK_WIDGET(overlay));
   gtk_container_add_with_properties(GTK_CONTAINER(vbox), entry,
                                     "expand", FALSE,
                                     NULL);
   window = g_object_new(GTK_TYPE_WINDOW,
                         "child", vbox,
                         "title", "main.c",
                         "window-position", GTK_WIN_POS_CENTER,
                         NULL);
   gtk_window_set_default_size(window, 720, 800);
   g_signal_connect(window, "key-press-event", G_CALLBACK(window_key_press),
                    sourceview);
   g_signal_connect(window, "delete-event", gtk_main_quit, NULL);
   gtk_window_present(window);

   gtk_main();

   return 0;
}
