/* gb-source-gutter-renderer-changes.c
 *
 * Copyright (C) 2013 Christian Hergert <christian@hergert.me>
 *
 * This file is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <glib/gi18n.h>

#include "gb-source-gutter-renderer-changes.h"

G_DEFINE_TYPE(GbSourceGutterRendererChanges,
              gb_source_gutter_renderer_changes,
              GTK_SOURCE_TYPE_GUTTER_RENDERER)

struct _GbSourceGutterRendererChangesPrivate
{
   GbChangeTracker *change_tracker;

   GdkRGBA rgba_added;
   GdkRGBA rgba_changed;
};

enum
{
   PROP_0,
   PROP_CHANGE_TRACKER,
   LAST_PROP
};

static GParamSpec *gParamSpecs[LAST_PROP];

GtkSourceGutterRenderer *
gb_source_gutter_renderer_changes_new (GbChangeTracker *change_tracker)
{
   return g_object_new(GB_TYPE_SOURCE_GUTTER_RENDERER_CHANGES,
                       "change-tracker", change_tracker,
                       NULL);
}

static void
gb_source_gutter_renderer_changes_draw (GtkSourceGutterRenderer      *renderer,
                                        cairo_t                      *cr,
                                        GdkRectangle                 *bg_area,
                                        GdkRectangle                 *cell_area,
                                        GtkTextIter                  *begin,
                                        GtkTextIter                  *end,
                                        GtkSourceGutterRendererState  state)
{
   GbSourceGutterRendererChangesPrivate *priv;
   GbChangeTrackerState mode;
   guint line;

   priv = GB_SOURCE_GUTTER_RENDERER_CHANGES(renderer)->priv;

   line = gtk_text_iter_get_line(begin);
   mode = gb_change_tracker_get_line_state(priv->change_tracker, line);

   switch (mode) {
   case GB_CHANGE_TRACKER_NONE:
      break;
   case GB_CHANGE_TRACKER_ADDED:
      gdk_cairo_rectangle(cr, cell_area);
      gdk_cairo_set_source_rgba(cr, &priv->rgba_added);
      cairo_fill(cr);
      break;
   case GB_CHANGE_TRACKER_CHANGED:
      gdk_cairo_rectangle(cr, cell_area);
      gdk_cairo_set_source_rgba(cr, &priv->rgba_changed);
      cairo_fill(cr);
      break;
   default:
      break;
   }
}

static void
gb_source_gutter_renderer_changes_finalize (GObject *object)
{
   GbSourceGutterRendererChangesPrivate *priv;

   priv = GB_SOURCE_GUTTER_RENDERER_CHANGES(object)->priv;

   g_clear_object(&priv->change_tracker);

   G_OBJECT_CLASS(gb_source_gutter_renderer_changes_parent_class)->finalize(object);
}

static void
gb_source_gutter_renderer_changes_get_property (GObject    *object,
                                                guint       prop_id,
                                                GValue     *value,
                                                GParamSpec *pspec)
{
   GbSourceGutterRendererChanges *changes = GB_SOURCE_GUTTER_RENDERER_CHANGES(object);

   switch (prop_id) {
   case PROP_CHANGE_TRACKER:
      g_value_set_object(value, changes->priv->change_tracker);
      break;
   default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID(object, prop_id, pspec);
   }
}

static void
gb_source_gutter_renderer_changes_set_property (GObject      *object,
                                                guint         prop_id,
                                                const GValue *value,
                                                GParamSpec   *pspec)
{
   GbSourceGutterRendererChanges *changes = GB_SOURCE_GUTTER_RENDERER_CHANGES(object);

   switch (prop_id) {
   case PROP_CHANGE_TRACKER:
      changes->priv->change_tracker = g_value_dup_object(value);
      g_signal_connect_swapped(changes->priv->change_tracker,
                               "changed",
                               G_CALLBACK(gtk_source_gutter_renderer_queue_draw),
                               changes);
      break;
   default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID(object, prop_id, pspec);
   }
}

static void
gb_source_gutter_renderer_changes_class_init (GbSourceGutterRendererChangesClass *klass)
{
   GtkSourceGutterRendererClass *renderer_class;
   GObjectClass *object_class;

   object_class = G_OBJECT_CLASS(klass);
   object_class->finalize = gb_source_gutter_renderer_changes_finalize;
   object_class->get_property = gb_source_gutter_renderer_changes_get_property;
   object_class->set_property = gb_source_gutter_renderer_changes_set_property;
   g_type_class_add_private(object_class, sizeof(GbSourceGutterRendererChangesPrivate));

   renderer_class = GTK_SOURCE_GUTTER_RENDERER_CLASS(klass);
   renderer_class->draw = gb_source_gutter_renderer_changes_draw;

   gParamSpecs[PROP_CHANGE_TRACKER] =
      g_param_spec_object("change-tracker",
                          _("Change Tracker"),
                          _("The change tracker to check for changes."),
                          GB_TYPE_CHANGE_TRACKER,
                          G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY | G_PARAM_STATIC_STRINGS);
   g_object_class_install_property(object_class, PROP_CHANGE_TRACKER,
                                   gParamSpecs[PROP_CHANGE_TRACKER]);
}

static void
gb_source_gutter_renderer_changes_init (GbSourceGutterRendererChanges *changes)
{
   changes->priv = G_TYPE_INSTANCE_GET_PRIVATE(changes,
                                               GB_TYPE_SOURCE_GUTTER_RENDERER_CHANGES,
                                               GbSourceGutterRendererChangesPrivate);

   gdk_rgba_parse(&changes->priv->rgba_added, "#8ae234");
   gdk_rgba_parse(&changes->priv->rgba_changed, "#fcaf3e");
}
