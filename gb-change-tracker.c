/* gb-change-tracker.c
 *
 * Copyright (C) 2013 Christian Hergert <christian@hergert.me>
 *
 * This file is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#define _GNU_SOURCE

#include <ctype.h>
#include <errno.h>
#include <glib/gi18n.h>
#include <glib/gstdio.h>
#include <stdlib.h>
#include <unistd.h>

#include "gb-change-tracker.h"

G_DEFINE_TYPE(GbChangeTracker, gb_change_tracker, G_TYPE_OBJECT)

struct _GbChangeTrackerPrivate
{
   GtkTextBuffer *buffer;
   guint          insert_handler;
   guint          delete_handler;
   GArray        *bytes;
};

enum
{
   PROP_0,
   PROP_BUFFER,
   LAST_PROP
};

enum
{
   CHANGED,
   LAST_SIGNAL
};

static GParamSpec *gParamSpecs[LAST_PROP];
static guint       gSignals[LAST_SIGNAL];

static gchar *
next_line (gchar *buf)
{
   while (*buf && *buf != '\n') {
      buf++;
   }

   if (*buf) {
      return &buf[1];
   }

   return NULL;
}

static gint
count_newlines (const gchar *str)
{
   gint count = 0;

   for (; *str; str++) {
      if (*str == '\n') {
         count++;
      }
   }

   return count;
}

static void
parse_line (const gchar *c,
            gint        *src_begin,
            gint        *src_end,
            gchar       *mode,
            gint        *dst_begin,
            gint        *dst_end)
{
   gint vars[4] = { -1, -1, -1, -1 };
   gint i = 0;

   *mode = 0;

   for (; i < 4 && *c != '\n' && *c; c++) {
      switch (*c) {
      case '0':
      case '1':
      case '2':
      case '3':
      case '4':
      case '5':
      case '6':
      case '7':
      case '8':
      case '9':
         if (vars[i] == -1) {
            vars[i] = 0;
         }
         vars[i] = (vars[i] * 10) + (*c - '0');
         break;
      case 'a':
      case 'c':
      case 'd':
         *mode = *c;
         i = MAX(1, i);
      case ',':
         i++;
         break;
      default:
         break;
      }
   }

   if (vars[1] == -1) {
      vars[1] = vars[0];
   }

   if (vars[3] == -1) {
      vars[3] = vars[2];
   }

   *src_begin = vars[0];
   *src_end = vars[1];
   *dst_begin = vars[2];
   *dst_end = vars[3];
}

static void
set_line (GbChangeTracker      *tracker,
          guint                 line,
          GbChangeTrackerState  state)
{
   GbChangeTrackerPrivate *priv = tracker->priv;
   guint8 *data;
   guint8 value = (guint8)state;

   data = (guint8 *)priv->bytes->data;

   if (data[line] != (guint8)GB_CHANGE_TRACKER_ADDED) {
      data[line] = value;
   }
}

static void
gb_change_tracker_rebuild (GbChangeTracker *tracker,
                           const gchar     *filename,
                           const gchar     *contents)
{
   GbChangeTrackerPrivate *priv;
   GError *error = NULL;
   gchar **argv;
   gchar *path;
   gchar *iter;
   gchar *standard_output = NULL;
   gint fd;
   gint exit_status;
   gint lines;
   gchar mode;
   gint src_begin;
   gint src_end;
   gint dst_begin;
   gint dst_end;

   priv = tracker->priv;

   if (-1 == (fd = g_file_open_tmp("XXXXXX.change-tracker", &path, &error))) {
      g_warning("%s", error->message);
      g_error_free(error);
      return;
   }

   TEMP_FAILURE_RETRY(write(fd, contents, strlen(contents)));

   argv = g_new0(gchar*, 5);
   argv[0] = g_strdup("diff");
   argv[1] = g_strdup("-d");
   argv[2] = g_strdup(filename);
   argv[3] = path;
   argv[4] = NULL;

   if (!g_spawn_sync(".", argv, NULL,
                     G_SPAWN_SEARCH_PATH_FROM_ENVP,
                     NULL,
                     NULL,
                     &standard_output,
                     NULL,
                     &exit_status,
                     &error)) {
      g_warning("%s", error->message);
      goto cleanup;
   }

   lines = count_newlines(contents);
   if (priv->bytes->len) {
      g_array_remove_range(priv->bytes, 0, priv->bytes->len - 1);
   }
   g_array_set_size(priv->bytes, lines + 1);

   for (iter = standard_output; iter; iter = next_line(iter)) {
      if (isdigit(*iter)) {
         GbChangeTrackerState state;
         gint i;

         parse_line(iter, &src_begin, &src_end, &mode, &dst_begin, &dst_end);

#if 0
         g_print ("%d %d %c %d %d\n",
                  src_begin, src_end, mode, dst_begin, dst_end);
#endif

         if (mode == 'c' || mode == 'a') {
            state = (mode == 'c') ?
               GB_CHANGE_TRACKER_CHANGED : GB_CHANGE_TRACKER_ADDED;
            for (i = dst_begin; i <= dst_end; i++) {
               set_line(tracker, i - 1, state);
            }
         }
      }
   }

   g_signal_emit(tracker, gSignals[CHANGED], 0);

cleanup:
   g_unlink(path);
   g_clear_error(&error);
   g_strfreev(argv);
   g_free(standard_output);
   close(fd);
}

GbChangeTracker *
gb_change_tracker_new (GtkTextBuffer *buffer)
{
   return g_object_new(GB_TYPE_CHANGE_TRACKER,
                       "buffer", buffer,
                       NULL);
}

void
gb_change_tracker_reset (GbChangeTracker *tracker)
{
   GbChangeTrackerPrivate *priv;

   g_return_if_fail(GB_IS_CHANGE_TRACKER(tracker));

   priv = tracker->priv;

   g_array_remove_range(priv->bytes, 0, priv->bytes->len - 1);
}

GbChangeTrackerState
gb_change_tracker_get_line_state (GbChangeTracker *tracker,
                                  guint            line)
{
   GbChangeTrackerPrivate *priv;
   guint8 value;

   g_return_val_if_fail(GB_IS_CHANGE_TRACKER(tracker), 0);

   priv = tracker->priv;

   if (priv->bytes->len > line) {
      value = g_array_index(priv->bytes, guint8, line);
      return (GbChangeTrackerState)value;
   }

   return GB_CHANGE_TRACKER_NONE;
}

static void
do_update (GbChangeTracker *tracker,
           GtkTextBuffer   *buffer)
{
   GtkTextIter begin;
   GtkTextIter end;
   gchar *text;

   gtk_text_buffer_get_bounds(buffer, &begin, &end);
   text = gtk_text_buffer_get_text(buffer, &begin, &end, TRUE);
   gb_change_tracker_rebuild(tracker, "main.c", text);
   g_free(text);
}

static void
gb_change_tracker_insert_text (GtkTextBuffer   *buffer,
                               GtkTextIter     *location,
                               gchar           *text,
                               gint             length,
                               GbChangeTracker *tracker)
{
   gint line;

   if (strstr(text, "\n")) {
      do_update(tracker, buffer);
   } else {
      line = gtk_text_iter_get_line(location);
      set_line(tracker, line, GB_CHANGE_TRACKER_CHANGED);
   }
}

static void
gb_change_tracker_delete_range (GtkTextBuffer   *buffer,
                                GtkTextIter     *begin,
                                GtkTextIter     *end,
                                GbChangeTracker *tracker)
{
   do_update(tracker, buffer);
}

static void
gb_change_tracker_set_buffer (GbChangeTracker *tracker,
                              GtkTextBuffer   *buffer)
{
   GbChangeTrackerPrivate *priv;

   g_return_if_fail(GB_IS_CHANGE_TRACKER(tracker));
   g_return_if_fail(GTK_IS_TEXT_BUFFER(buffer));

   priv = tracker->priv;

   priv->insert_handler =
      g_signal_connect_after(buffer, "insert-text",
                             G_CALLBACK(gb_change_tracker_insert_text),
                             tracker);

   priv->delete_handler =
      g_signal_connect_after(buffer, "delete-range",
                             G_CALLBACK(gb_change_tracker_delete_range),
                             tracker);
}

static void
gb_change_tracker_finalize (GObject *object)
{
   GbChangeTrackerPrivate *priv;

   priv = GB_CHANGE_TRACKER(object)->priv;

   g_signal_handler_disconnect(priv->buffer, priv->insert_handler);
   g_signal_handler_disconnect(priv->buffer, priv->delete_handler);
   g_clear_object(&priv->buffer);
   g_clear_pointer(&priv->bytes, g_array_unref);

   G_OBJECT_CLASS(gb_change_tracker_parent_class)->finalize(object);
}

static void
gb_change_tracker_get_property (GObject    *object,
                                guint       prop_id,
                                GValue     *value,
                                GParamSpec *pspec)
{
   GbChangeTracker *tracker = GB_CHANGE_TRACKER(object);

   switch (prop_id) {
   case PROP_BUFFER:
      g_value_set_object(value, tracker->priv->buffer);
      break;
   default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID(object, prop_id, pspec);
   }
}

static void
gb_change_tracker_set_property (GObject      *object,
                                guint         prop_id,
                                const GValue *value,
                                GParamSpec   *pspec)
{
   GbChangeTracker *tracker = GB_CHANGE_TRACKER(object);

   switch (prop_id) {
   case PROP_BUFFER:
      gb_change_tracker_set_buffer(tracker, g_value_get_object(value));
      break;
   default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID(object, prop_id, pspec);
   }
}

static void
gb_change_tracker_class_init (GbChangeTrackerClass *klass)
{
   GObjectClass *object_class;

   object_class = G_OBJECT_CLASS(klass);
   object_class->finalize = gb_change_tracker_finalize;
   object_class->get_property = gb_change_tracker_get_property;
   object_class->set_property = gb_change_tracker_set_property;
   g_type_class_add_private(object_class, sizeof(GbChangeTrackerPrivate));

   gParamSpecs[PROP_BUFFER] =
      g_param_spec_object("buffer",
                          _("Buffer"),
                          _("The GtkTextBuffer to track."),
                          GTK_TYPE_TEXT_BUFFER,
                          G_PARAM_READWRITE |
                          G_PARAM_STATIC_STRINGS |
                          G_PARAM_CONSTRUCT_ONLY);
   g_object_class_install_property(object_class, PROP_BUFFER,
                                   gParamSpecs[PROP_BUFFER]);

   gSignals[CHANGED] = g_signal_new("changed",
                                    GB_TYPE_CHANGE_TRACKER,
                                    G_SIGNAL_RUN_FIRST,
                                    0,
                                    NULL,
                                    NULL,
                                    g_cclosure_marshal_generic,
                                    G_TYPE_NONE,
                                    0);
}

static void
gb_change_tracker_init (GbChangeTracker *tracker)
{
   tracker->priv = G_TYPE_INSTANCE_GET_PRIVATE(tracker,
                                               GB_TYPE_CHANGE_TRACKER,
                                               GbChangeTrackerPrivate);
   tracker->priv->bytes = g_array_new(FALSE, TRUE, sizeof(guint8));
}
