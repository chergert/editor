/* gb-source-view-state-insert.c
 *
 * Copyright (C) 2013 Christian Hergert <christian@hergert.me>
 *
 * This file is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <glib/gi18n.h>

#include "gb-source-view-state-insert.h"
#include "gb-source-view-state-snippet.h"

G_DEFINE_TYPE(GbSourceViewStateInsert, gb_source_view_state_insert,
              GB_TYPE_SOURCE_VIEW_STATE)

struct _GbSourceViewStateInsertPrivate
{
   guint key_press_handler;
};

enum
{
   PROP_0,
   LAST_PROP
};

//static GParamSpec *gParamSpecs[LAST_PROP];

GbSourceViewState *
gb_source_view_state_insert_new (void)
{
   return g_object_new(GB_TYPE_SOURCE_VIEW_STATE_INSERT, NULL);
}

static void
gb_source_view_state_insert_get_word_bounds (GbSourceViewStateInsert *insert,
                                             GbSourceView            *view,
                                             GtkTextIter             *begin,
                                             GtkTextIter             *end)
{
   GtkTextBuffer *buffer;
   GtkTextMark *mark;
   gunichar c;

   buffer = gtk_text_view_get_buffer(GTK_TEXT_VIEW(view));
   mark = gtk_text_buffer_get_insert(buffer);
   gtk_text_buffer_get_iter_at_mark(buffer, end, mark);
   gtk_text_iter_assign(begin, end);
   while (gtk_text_iter_backward_char(begin)) {
      c = gtk_text_iter_get_char(begin);
      if (c == '_' || g_unichar_isalnum(c)) {
         continue;
      }
      gtk_text_iter_forward_char(begin);
      break;
   }
}

static gchar *
gb_source_view_state_insert_get_word (GbSourceViewStateInsert *insert,
                                      GbSourceView            *view)
{
   GtkTextBuffer *buffer;
   GtkTextIter begin;
   GtkTextIter end;

   gb_source_view_state_insert_get_word_bounds(insert, view, &begin, &end);
   buffer = gtk_text_view_get_buffer(GTK_TEXT_VIEW(view));
   return gtk_text_buffer_get_text(buffer, &begin, &end, TRUE);
}

static gboolean
gb_source_view_state_insert_key_press_event (GbSourceView            *view,
                                             GdkEventKey             *key,
                                             GbSourceViewStateInsert *insert)
{
   GbSourceViewSnippets *snippets;
   GbSourceViewSnippet *snippet;
   GbSourceViewState *state;
   GtkTextBuffer *buffer;
   GtkTextIter begin;
   GtkTextIter end;
   gchar *word;

   switch (key->keyval) {
   case GDK_KEY_Tab:
      snippets = gb_source_view_get_snippets(view);
      word = gb_source_view_state_insert_get_word(insert, view);
      if ((snippet = gb_source_view_snippets_lookup(snippets, word))) {
         buffer = gtk_text_view_get_buffer(GTK_TEXT_VIEW(view));
         gb_source_view_state_insert_get_word_bounds(insert, view, &begin, &end);
         gtk_text_buffer_delete(buffer, &begin, &end);
         state = g_object_new(GB_TYPE_SOURCE_VIEW_STATE_SNIPPET,
                              "snippet", snippet,
                              NULL);
         g_object_set(view, "state", state, NULL);
         g_object_unref(state);
         g_object_unref(snippet);
      }
      g_free(word);
      return !!snippet;
   default:
      return FALSE;
   }
}

static void
gb_source_view_state_insert_load (GbSourceViewState *state,
                                  GbSourceView      *view)
{
   GbSourceViewStateInsertPrivate *priv;
   GbSourceViewStateInsert *insert = (GbSourceViewStateInsert *)state;

   g_return_if_fail(GB_IS_SOURCE_VIEW_STATE_INSERT(insert));
   g_return_if_fail(GB_IS_SOURCE_VIEW(view));

   priv = insert->priv;

   priv->key_press_handler =
      g_signal_connect(view, "key-press-event",
                       G_CALLBACK(gb_source_view_state_insert_key_press_event),
                       state);
}

static void
gb_source_view_state_insert_unload (GbSourceViewState *state,
                                    GbSourceView      *view)
{
   GbSourceViewStateInsertPrivate *priv;
   GbSourceViewStateInsert *insert = (GbSourceViewStateInsert *)state;

   g_return_if_fail(GB_IS_SOURCE_VIEW_STATE_INSERT(insert));
   g_return_if_fail(GB_IS_SOURCE_VIEW(view));

   priv = insert->priv;

   g_signal_handler_disconnect(view, priv->key_press_handler);
   priv->key_press_handler = 0;
}

static void
gb_source_view_state_insert_finalize (GObject *object)
{
   G_OBJECT_CLASS(gb_source_view_state_insert_parent_class)->finalize(object);
}

static void
gb_source_view_state_insert_get_property (GObject    *object,
                                    guint       prop_id,
                                    GValue     *value,
                                    GParamSpec *pspec)
{
   //GbSourceViewStateInsert *insert = GB_SOURCE_VIEW_STATE_INSERT(object);

   switch (prop_id) {
   default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID(object, prop_id, pspec);
   }
}

static void
gb_source_view_state_insert_set_property (GObject      *object,
                                    guint         prop_id,
                                    const GValue *value,
                                    GParamSpec   *pspec)
{
   //GbSourceViewStateInsert *insert = GB_SOURCE_VIEW_STATE_INSERT(object);

   switch (prop_id) {
   default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID(object, prop_id, pspec);
   }
}

static void
gb_source_view_state_insert_class_init (GbSourceViewStateInsertClass *klass)
{
   GObjectClass *object_class;
   GbSourceViewStateClass *state_class;

   object_class = G_OBJECT_CLASS(klass);
   object_class->finalize = gb_source_view_state_insert_finalize;
   object_class->get_property = gb_source_view_state_insert_get_property;
   object_class->set_property = gb_source_view_state_insert_set_property;
   g_type_class_add_private(object_class, sizeof(GbSourceViewStateInsertPrivate));

   state_class = GB_SOURCE_VIEW_STATE_CLASS(klass);
   state_class->load = gb_source_view_state_insert_load;
   state_class->unload = gb_source_view_state_insert_unload;
}

static void
gb_source_view_state_insert_init (GbSourceViewStateInsert *insert)
{
   insert->priv = G_TYPE_INSTANCE_GET_PRIVATE(insert,
                                              GB_TYPE_SOURCE_VIEW_STATE_INSERT,
                                              GbSourceViewStateInsertPrivate);
}
