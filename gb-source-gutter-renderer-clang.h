/* gb-source-gutter-renderer-clang.h
 *
 * Copyright (C) 2013 Christian Hergert <christian@hergert.me>
 *
 * This file is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef GB_SOURCE_GUTTER_RENDERER_CLANG_H
#define GB_SOURCE_GUTTER_RENDERER_CLANG_H

#include <gtksourceview/gtksourcegutterrenderer.h>

#include "gb-clang.h"

G_BEGIN_DECLS

#define GB_TYPE_SOURCE_GUTTER_RENDERER_CLANG            (gb_source_gutter_renderer_clang_get_type())
#define GB_SOURCE_GUTTER_RENDERER_CLANG(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj), GB_TYPE_SOURCE_GUTTER_RENDERER_CLANG, GbSourceGutterRendererClang))
#define GB_SOURCE_GUTTER_RENDERER_CLANG_CONST(obj)      (G_TYPE_CHECK_INSTANCE_CAST ((obj), GB_TYPE_SOURCE_GUTTER_RENDERER_CLANG, GbSourceGutterRendererClang const))
#define GB_SOURCE_GUTTER_RENDERER_CLANG_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass),  GB_TYPE_SOURCE_GUTTER_RENDERER_CLANG, GbSourceGutterRendererClangClass))
#define GB_IS_SOURCE_GUTTER_RENDERER_CLANG(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), GB_TYPE_SOURCE_GUTTER_RENDERER_CLANG))
#define GB_IS_SOURCE_GUTTER_RENDERER_CLANG_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass),  GB_TYPE_SOURCE_GUTTER_RENDERER_CLANG))
#define GB_SOURCE_GUTTER_RENDERER_CLANG_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS ((obj),  GB_TYPE_SOURCE_GUTTER_RENDERER_CLANG, GbSourceGutterRendererClangClass))

typedef struct _GbSourceGutterRendererClang        GbSourceGutterRendererClang;
typedef struct _GbSourceGutterRendererClangClass   GbSourceGutterRendererClangClass;
typedef struct _GbSourceGutterRendererClangPrivate GbSourceGutterRendererClangPrivate;

struct _GbSourceGutterRendererClang
{
   GtkSourceGutterRenderer parent;

   /*< private >*/
   GbSourceGutterRendererClangPrivate *priv;
};

struct _GbSourceGutterRendererClangClass
{
   GtkSourceGutterRendererClass parent_class;
};

GType                    gb_source_gutter_renderer_clang_get_type  (void) G_GNUC_CONST;
GtkSourceGutterRenderer *gb_source_gutter_renderer_clang_new       (GbClang     *clang,
                                                                    const gchar *path);

G_END_DECLS

#endif /* GB_SOURCE_GUTTER_RENDERER_CLANG_H */
