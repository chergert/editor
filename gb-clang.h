/* gb-clang.h
 *
 * Copyright (C) 2013 Christian Hergert <christian@hergert.me>
 *
 * This file is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef GB_CLANG_H
#define GB_CLANG_H

#include <glib-object.h>
#include <gtk/gtk.h>

G_BEGIN_DECLS

#define GB_TYPE_CLANG            (gb_clang_get_type())
#define GB_CLANG(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj), GB_TYPE_CLANG, GbClang))
#define GB_CLANG_CONST(obj)      (G_TYPE_CHECK_INSTANCE_CAST ((obj), GB_TYPE_CLANG, GbClang const))
#define GB_CLANG_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass),  GB_TYPE_CLANG, GbClangClass))
#define GB_IS_CLANG(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), GB_TYPE_CLANG))
#define GB_IS_CLANG_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass),  GB_TYPE_CLANG))
#define GB_CLANG_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS ((obj),  GB_TYPE_CLANG, GbClangClass))

typedef struct _GbClang        GbClang;
typedef struct _GbClangClass   GbClangClass;
typedef struct _GbClangPrivate GbClangPrivate;

struct _GbClang
{
   GObject parent;

   /*< private >*/
   GbClangPrivate *priv;
};

struct _GbClangClass
{
   GObjectClass parent_class;
};

GType    gb_clang_get_type    (void) G_GNUC_CONST;
GbClang *gb_clang_new         (void);
void     gb_clang_add_buffer  (GbClang       *clang,
                               const gchar   *filename,
                               GtkTextBuffer *buffer,
                               gchar         **args);
void     gb_clang_translate   (GbClang       *clang,
                               const gchar   *filename,
                               GtkTextBuffer *buffer);
void     gb_clang_highlight   (GbClang       *clang,
                               const gchar   *filename,
                               GtkTextBuffer *buffer);
gchar   *gb_clang_get_tooltip (GbClang       *clang,
                               const gchar   *filename,
                               GtkTextBuffer *buffer,
                               GtkTextIter   *begin,
                               GtkTextIter   *end);
gboolean  gb_clang_get_has_diagnostic (GbClang        *clang,
                                       const gchar    *filename,
                                       GtkTextBuffer  *buffer,
                                       GtkTextIter    *begin,
                                       GtkTextIter    *end);
void      gb_clang_set_args           (GbClang        *clang,
                                       const gchar    *path,
                                       gchar         **argv);

G_END_DECLS

#endif /* GB_CLANG_H */
