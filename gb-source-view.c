/* gb-source-view.c
 *
 * Copyright (C) 2013 Christian Hergert <christian@hergert.me>
 *
 * This file is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <glib/gi18n.h>

#include "gb-source-view.h"
#include "gb-source-view-snippets.h"
#include "gb-source-view-state.h"
#include "gb-source-view-state-insert.h"

#ifndef g_str_empty0
#define g_str_empty0(s) (!(s) || !(s)[0])
#endif

G_DEFINE_TYPE(GbSourceView, gb_source_view, GTK_SOURCE_TYPE_VIEW)

struct _GbSourceViewPrivate
{
   GbSourceViewSnippets *snippets;
   GbSourceViewState    *state;
   gboolean              search_case_sensitive;
   GRegex               *search_regex;
   gchar                *search_text;
   gboolean              has_matches;
};

enum
{
   PROP_0,
   PROP_HAS_MATCHES,
   PROP_SEARCH_CASE_SENSITIVE,
   PROP_SEARCH_REGEX,
   PROP_SEARCH_TEXT,
   PROP_SNIPPETS,
   PROP_STATE,
   LAST_PROP
};

static GParamSpec *gParamSpecs[LAST_PROP];

GtkWidget *
gb_source_view_new (void)
{
   return g_object_new(GB_TYPE_SOURCE_VIEW, NULL);
}

gboolean
gb_source_view_get_search_case_sensitive (GbSourceView *view)
{
   g_return_val_if_fail(GB_IS_SOURCE_VIEW(view), FALSE);
   return view->priv->search_case_sensitive;
}

void
gb_source_view_set_search_case_sensitive (GbSourceView *view,
                                          gboolean      search_case_sensitive)
{
   g_return_if_fail(GB_IS_SOURCE_VIEW(view));
   view->priv->search_case_sensitive = search_case_sensitive;
   g_object_notify_by_pspec(G_OBJECT(view),
                            gParamSpecs[PROP_SEARCH_CASE_SENSITIVE]);
}

GtkTextTag *
gb_source_view_ref_search_tag (GbSourceView *view)
{
   GtkTextTagTable *tag_table;
   GtkTextBuffer *buffer;
   GtkTextTag *search_tag;

   g_return_val_if_fail(GB_IS_SOURCE_VIEW(view), NULL);

   buffer = gtk_text_view_get_buffer(GTK_TEXT_VIEW(view));
   tag_table = gtk_text_buffer_get_tag_table(buffer);
   if (!(search_tag = gtk_text_tag_table_lookup(tag_table, "search-tag"))) {
      search_tag = g_object_new(GTK_TYPE_TEXT_TAG,
                                "name", "search-tag",
                                "background", "#fce94f",
                                "foreground", "#2e3436",
                                NULL);
      gtk_text_tag_table_add(tag_table, search_tag);
      return search_tag;
   }

   return g_object_ref(search_tag);
}

void
gb_source_view_move_next_match (GbSourceView *view)
{
   GtkTextBuffer *buffer;
   GdkRectangle rect;
   GtkTextIter iter;
   GtkTextTag *search_tag;
   gboolean wrapped = FALSE;

   g_return_if_fail(GB_IS_SOURCE_VIEW(view));

   /*
    * TODO: Track current match, so move next match jumps past it next time.
    */

   search_tag = gb_source_view_ref_search_tag(view);
   gtk_text_view_get_visible_rect(GTK_TEXT_VIEW(view), &rect);
   gtk_text_view_get_iter_at_location(GTK_TEXT_VIEW(view),
                                      &iter, rect.x, rect.y);

again:
   if (!gtk_text_iter_begins_tag(&iter, search_tag)) {
      if (gtk_text_iter_forward_to_tag_toggle(&iter, search_tag)) {
         if (!gtk_text_iter_begins_tag(&iter, search_tag)) {
            gtk_text_iter_backward_to_tag_toggle(&iter, search_tag);
         }
         gtk_text_view_scroll_to_iter(GTK_TEXT_VIEW(view), &iter,
                                      0.25, FALSE, 0, 0);
         goto cleanup;
      }
      if (!wrapped) {
         buffer = gtk_text_view_get_buffer(GTK_TEXT_VIEW(view));
         gtk_text_buffer_get_start_iter(buffer, &iter);
         wrapped = TRUE;
         goto again;
      }
   }

cleanup:
   g_object_unref(search_tag);
}

static void
gb_source_view_update_search (GbSourceView *view)
{
   GbSourceViewPrivate *priv;
   GRegexCompileFlags flags = 0;
   GtkTextBuffer *buffer;
   GtkTextIter begin;
   GtkTextIter end;
   GMatchInfo *match_info = NULL;
   GtkTextTag *search_tag;
   gboolean has_matches = FALSE;
   GRegex *regex = NULL;
   gchar *text;
   gchar *escaped;

   g_assert(GB_IS_SOURCE_VIEW(view));

   priv = view->priv;

   buffer = gtk_text_view_get_buffer(GTK_TEXT_VIEW(view));
   gtk_text_buffer_get_bounds(buffer, &begin, &end);

   search_tag = gb_source_view_ref_search_tag(view);
   gtk_text_buffer_remove_tag(buffer, search_tag, &begin, &end);

   if (g_str_empty0(priv->search_text) && !priv->search_regex) {
      goto cleanup;
   }

   if (priv->search_text) {
      if (!priv->search_case_sensitive) {
         flags = G_REGEX_CASELESS;
      }
      escaped = g_regex_escape_string(priv->search_text, -1);
      regex = g_regex_new(escaped, flags, 0, NULL);
      g_free(escaped);
   } else if (priv->search_regex) {
      regex = g_regex_ref(priv->search_regex);
   }

   if (regex) {
      text = gtk_text_buffer_get_text(buffer, &begin, &end, TRUE);
      if (g_regex_match(regex, text, 0, &match_info)) {
         guint count;
         guint i;
         gint begin_pos;
         gint end_pos;

         do {
            count = g_match_info_get_match_count(match_info);
            for (i = 0; i < count; i++) {
               if (g_match_info_fetch_pos(match_info, i, &begin_pos, &end_pos)) {
                  gtk_text_buffer_get_iter_at_offset(buffer, &begin, begin_pos);
                  gtk_text_buffer_get_iter_at_offset(buffer, &end, end_pos);
                  gtk_text_buffer_apply_tag(buffer, search_tag, &begin, &end);
                  has_matches = TRUE;
               }
            }
         } while (g_match_info_next(match_info, NULL));
      }
      g_match_info_free(match_info);
      g_regex_unref(regex);
      g_free(text);
   }

cleanup:
   if (priv->has_matches != has_matches) {
      priv->has_matches = has_matches;
      g_object_notify_by_pspec(G_OBJECT(view), gParamSpecs[PROP_HAS_MATCHES]);
   }

   gtk_widget_queue_draw(GTK_WIDGET(view));
   g_object_unref(search_tag);
}

GRegex *
gb_source_view_get_search_regex (GbSourceView *view)
{
   g_return_val_if_fail(GB_IS_SOURCE_VIEW(view), NULL);
   return view->priv->search_regex;
}

void
gb_source_view_set_search_regex (GbSourceView *view,
                                 GRegex       *search_regex)
{
   GbSourceViewPrivate *priv;

   g_return_if_fail(GB_IS_SOURCE_VIEW(view));

   priv = view->priv;

   g_clear_pointer(&priv->search_text, g_free);
   g_clear_pointer(&priv->search_regex, g_regex_unref);

   if (search_regex) {
      priv->search_regex = g_regex_ref(search_regex);
   }

   gb_source_view_update_search(view);

   if (priv->has_matches) {
      gb_source_view_move_next_match(view);
   }

   g_object_notify_by_pspec(G_OBJECT(view), gParamSpecs[PROP_SEARCH_REGEX]);
   g_object_notify_by_pspec(G_OBJECT(view), gParamSpecs[PROP_SEARCH_TEXT]);
}

const gchar *
gb_source_view_get_search_text (GbSourceView *view)
{
   g_return_val_if_fail(GB_IS_SOURCE_VIEW(view), NULL);
   return view->priv->search_text;
}

void
gb_source_view_set_search_text (GbSourceView *view,
                                const gchar  *search_text)
{
   GbSourceViewPrivate *priv;

   g_return_if_fail(GB_IS_SOURCE_VIEW(view));

   priv = view->priv;

   g_clear_pointer(&priv->search_text, g_free);
   g_clear_pointer(&priv->search_regex, g_regex_unref);

   priv->search_text = g_strdup(search_text);

   gb_source_view_update_search(view);

   if (priv->has_matches) {
      gb_source_view_move_next_match(view);
   }

   g_object_notify_by_pspec(G_OBJECT(view), gParamSpecs[PROP_SEARCH_REGEX]);
   g_object_notify_by_pspec(G_OBJECT(view), gParamSpecs[PROP_SEARCH_TEXT]);
}

GbSourceViewSnippets *
gb_source_view_get_snippets (GbSourceView *view)
{
   g_return_val_if_fail(GB_IS_SOURCE_VIEW(view), NULL);
   return view->priv->snippets;
}

GbSourceViewState *
gb_source_view_get_state (GbSourceView *view)
{
   g_return_val_if_fail(GB_IS_SOURCE_VIEW(view), NULL);
   return view->priv->state;
}

void
gb_source_view_set_state (GbSourceView      *view,
                          GbSourceViewState *state)
{
   GbSourceViewPrivate *priv;

   g_return_if_fail(GB_IS_SOURCE_VIEW(view));
   g_return_if_fail(!state || GB_IS_SOURCE_VIEW_STATE(state));

   priv = view->priv;

   if (priv->state) {
      gb_source_view_state_unload(priv->state, view);
      g_clear_object(&priv->state);
   }

   if (state) {
      priv->state = g_object_ref(state);
      gb_source_view_state_load(priv->state, view);
   }

   g_object_notify_by_pspec(G_OBJECT(view), gParamSpecs[PROP_STATE]);
}

static void
gb_source_view_buffer_changed (GtkTextBuffer *buffer,
                               GbSourceView  *view)
{
   gb_source_view_update_search(view);
}

static void
gb_source_view_notify (GObject    *object,
                       GParamSpec *pspec)
{
   if (!g_strcmp0(pspec->name, "buffer")) {
      GtkTextBuffer *buffer;

      /*
       * TODO: How do we disconnect this signal when
       *       the property is changed?
       */

      buffer = gtk_text_view_get_buffer(GTK_TEXT_VIEW(object));
      g_signal_connect(buffer,
                       "changed",
                       G_CALLBACK(gb_source_view_buffer_changed),
                       object);
   }
}

static void
gb_source_view_dispose (GObject *object)
{
   gb_source_view_set_state(GB_SOURCE_VIEW(object), NULL);
   G_OBJECT_CLASS(gb_source_view_parent_class)->dispose(object);
}

static void
gb_source_view_finalize (GObject *object)
{
   GbSourceViewPrivate *priv;

   priv = GB_SOURCE_VIEW(object)->priv;

   g_clear_pointer(&priv->search_regex, g_regex_unref);
   g_clear_pointer(&priv->search_text, g_free);

   G_OBJECT_CLASS(gb_source_view_parent_class)->finalize(object);
}

static void
gb_source_view_get_property (GObject    *object,
                             guint       prop_id,
                             GValue     *value,
                             GParamSpec *pspec)
{
   GbSourceView *view = GB_SOURCE_VIEW(object);

   switch (prop_id) {
   case PROP_HAS_MATCHES:
      g_value_set_boolean(value, view->priv->has_matches);
      break;
   case PROP_SEARCH_CASE_SENSITIVE:
      g_value_set_boolean(value,
                          gb_source_view_get_search_case_sensitive(view));
      break;
   case PROP_SEARCH_REGEX:
      g_value_set_boxed(value, gb_source_view_get_search_regex(view));
      break;
   case PROP_SEARCH_TEXT:
      g_value_set_string(value, gb_source_view_get_search_text(view));
      break;
   case PROP_SNIPPETS:
      g_value_set_object(value, gb_source_view_get_snippets(view));
      break;
   case PROP_STATE:
      g_value_set_object(value, gb_source_view_get_state(view));
      break;
   default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID(object, prop_id, pspec);
   }
}

static void
gb_source_view_set_property (GObject      *object,
                             guint         prop_id,
                             const GValue *value,
                             GParamSpec   *pspec)
{
   GbSourceView *view = GB_SOURCE_VIEW(object);

   switch (prop_id) {
   case PROP_SEARCH_CASE_SENSITIVE:
      gb_source_view_set_search_case_sensitive(view,
                                               g_value_get_boolean(value));
      break;
   case PROP_SEARCH_REGEX:
      gb_source_view_set_search_regex(view, g_value_get_boxed(value));
      break;
   case PROP_SEARCH_TEXT:
      gb_source_view_set_search_text(view, g_value_get_string(value));
      break;
   case PROP_STATE:
      gb_source_view_set_state(view, g_value_get_object(value));
      break;
   default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID(object, prop_id, pspec);
   }
}

static void
gb_source_view_class_init (GbSourceViewClass *klass)
{
   GObjectClass *object_class;

   object_class = G_OBJECT_CLASS(klass);
   object_class->dispose = gb_source_view_dispose;
   object_class->finalize = gb_source_view_finalize;
   object_class->get_property = gb_source_view_get_property;
   object_class->notify = gb_source_view_notify;
   object_class->set_property = gb_source_view_set_property;
   g_type_class_add_private(object_class, sizeof(GbSourceViewPrivate));

   gParamSpecs[PROP_HAS_MATCHES] =
      g_param_spec_boolean("has-matches",
                          _("Has Matches"),
                          _("If there are search matches."),
                          FALSE,
                          G_PARAM_READABLE | G_PARAM_STATIC_STRINGS);
   g_object_class_install_property(object_class, PROP_HAS_MATCHES,
                                   gParamSpecs[PROP_HAS_MATCHES]);

   gParamSpecs[PROP_SEARCH_CASE_SENSITIVE] =
      g_param_spec_boolean("search-case-sensitive",
                          _("Search Case Sensitive"),
                          _("If the search should be case-sensitive."),
                          FALSE,
                          G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS);
   g_object_class_install_property(object_class, PROP_SEARCH_CASE_SENSITIVE,
                                   gParamSpecs[PROP_SEARCH_CASE_SENSITIVE]);

   gParamSpecs[PROP_SEARCH_REGEX] =
      g_param_spec_boxed("search-regex",
                         _("Search Regex"),
                         _("A regex to use for searching."),
                         G_TYPE_REGEX,
                         G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS);
   g_object_class_install_property(object_class, PROP_SEARCH_REGEX,
                                   gParamSpecs[PROP_SEARCH_REGEX]);

   gParamSpecs[PROP_SEARCH_TEXT] =
      g_param_spec_string("search-text",
                          _("Search Text"),
                          _("A text string to search for."),
                          NULL,
                          G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS);
   g_object_class_install_property(object_class, PROP_SEARCH_TEXT,
                                   gParamSpecs[PROP_SEARCH_TEXT]);

   gParamSpecs[PROP_SNIPPETS] =
      g_param_spec_object("snippets",
                          _("Snippets"),
                          _("The snippets for the source view."),
                          GB_TYPE_SOURCE_VIEW_SNIPPETS,
                          G_PARAM_READABLE | G_PARAM_STATIC_STRINGS);
   g_object_class_install_property(object_class, PROP_SNIPPETS,
                                   gParamSpecs[PROP_SNIPPETS]);

   gParamSpecs[PROP_STATE] =
      g_param_spec_object("state",
                          _("State"),
                          _("The current state machine state."),
                          GB_TYPE_SOURCE_VIEW_STATE,
                          G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS);
   g_object_class_install_property(object_class, PROP_STATE,
                                   gParamSpecs[PROP_STATE]);
}

static void
gb_source_view_init (GbSourceView *view)
{
   GbSourceViewState *state;

   view->priv = G_TYPE_INSTANCE_GET_PRIVATE(view,
                                            GB_TYPE_SOURCE_VIEW,
                                            GbSourceViewPrivate);

   view->priv->snippets = g_object_new(GB_TYPE_SOURCE_VIEW_SNIPPETS, NULL);

   state = gb_source_view_state_insert_new();
   gb_source_view_set_state(view, state);
   g_object_unref(state);
}
