#!/usr/bin/env python

import cairo
from gi.repository import Gb
from gi.repository import Gdk
from gi.repository import Gtk
from gi.repository import GtkSource
from gi.repository import Pango
import re

Snippets = {}
highlight = None

class SnippetState:
    buffer = None
    draw_handler = None
    handler = None
    mark_begin = None
    mark_end = None
    mark_handler = None
    snippet = None
    view = None
    current_param = None
    updating_linked_params = False

    def __init__(self, snippet):
        self.snippet = snippet

    def initialize(self, view):
        self.buffer = view.get_buffer()
        self.view = view
        self.draw_handler = self.view.connect('draw', self.on_view_draw)
        self.handler = self.view.connect('key-press-event',
                                         self.on_view_key_press_event)
        self.insert_handler = self.buffer.connect_after('insert-text',
                                                        self.on_buffer_insert_text)
        self.mark_handler = self.buffer.connect('mark-set', self.on_buffer_mark_set)
        location = self.buffer.get_iter_at_mark(self.buffer.get_insert())
        self.mark_begin = self.buffer.create_mark('snippet-begin', location, True)
        self.mark_end = self.buffer.create_mark('snippet-end', location, False)
        self.param_tag = self.buffer.props.tag_table.lookup('snippet-param')
        if not self.param_tag:
            self.param_tag = self.buffer.create_tag('snippet-param')
        self.insert_snippet()

    def shutdown(self, view):
        if self.handler:
            self.view.disconnect(self.handler)
            self.handler = None
        if self.draw_handler:
            self.view.disconnect(self.draw_handler)
            self.draw_handler = None
        if self.mark_handler:
            self.buffer.disconnect(self.mark_handler)
            self.mark_handler = None
        if self.insert_handler:
            self.buffer.disconnect(self.insert_handler)
            self.insert_handler = None
        self.buffer.delete_mark(self.mark_begin)
        self.buffer.delete_mark(self.mark_end)
        self.view.queue_draw()

    def on_buffer_insert_text(self, buffer, location, text, length):
        mark = buffer.create_mark(None, location, False)
        self.update_linked_params()
        iter = buffer.get_iter_at_mark(mark)
        location.assign(iter)
        buffer.delete_mark(mark)
        return False

    def on_buffer_mark_set(self, buffer, location, mark):
        # Escape from snippet mode if we have moved outside the region.
        if mark == buffer.get_insert():
            begin = self.buffer.get_iter_at_mark(self.mark_begin)
            end = self.buffer.get_iter_at_mark(self.mark_end)
            if end.compare(location) < 0 or begin.compare(location) > 0:
                self.view.set_state(InsertState())

    def _select_chunk(self, chunk):
        begin = self.buffer.get_iter_at_mark(chunk.begin)
        end = self.buffer.get_iter_at_mark(chunk.end)
        self.buffer.select_range(begin, end)

    def move_next_param(self):
        if self.current_param is None:
            self.current_param = -1

        self.current_param += 1
        while self.current_param < len(self.snippet.chunks):
            chunk = self.snippet.chunks[self.current_param]
            if chunk.begin and chunk.end and chunk.kind != Chunk.LINKED:
                self._select_chunk(chunk)
                if chunk.kind == Chunk.CURSOR:
                    self.view.set_state(InsertState())
                return True
            self.current_param += 1

        # No parameter was found, just move to the end and switch states.
        end = self.buffer.get_iter_at_mark(self.mark_end)
        self.buffer.select_range(end, end)
        self.view.set_state(InsertState())
        return True

    def move_prev_param(self):
        # Move to previous snippet.
        while self.current_param not in (0, None):
            self.current_param -= 1
            chunk = self.snippet.chunks[self.current_param]
            if chunk.begin and chunk.end:
                self._select_chunk(chunk)
                return True

        # Move to beginning of snippet.
        location = self.buffer.get_iter_at_mark(self.mark_begin)
        self.buffer.select_range(location, location)

        return False

    def translate_text(self, text):
        if self.view.props.insert_spaces_instead_of_tabs:
            spaces = ' ' * self.view.props.tab_width
            text = text.replace('\t', spaces)
        return text

    def insert_snippet(self):
        self.updating_linked_params = True

        location = self.buffer.get_iter_at_mark(self.buffer.get_insert())

        for chunk in self.snippet.chunks:
            if chunk.kind in (Chunk.TYPED_TEXT, Chunk.TEXT):
                text = self.translate_text(chunk.text)
                self.buffer.insert(location, text, -1)
            elif chunk.kind in (Chunk.PLACEHOLDER, Chunk.CURSOR, Chunk.LINKED):
                chunk.begin = location.get_offset()
                self.buffer.insert_with_tags(location, chunk.text or '', self.param_tag)
                chunk.end = location.get_offset()

        for chunk in self.snippet.chunks:
            if chunk.begin is not None:
                location = self.buffer.get_iter_at_offset(chunk.begin)
                chunk.begin = self.buffer.create_mark(None, location, True)

            if chunk.end is not None:
                location = self.buffer.get_iter_at_offset(chunk.end)
                chunk.end = self.buffer.create_mark(None, location, False)

        self.updating_linked_params = False

        self.update_linked_params()

        begin = self.buffer.get_iter_at_mark(self.mark_begin)
        self.buffer.move_mark(self.buffer.get_insert(), begin)
        return self.move_next_param()

    def update_linked_params(self):
        if self.updating_linked_params:
            return
        self.updating_linked_params = True
        for chunk in self.snippet.chunks:
            if chunk.kind == Chunk.LINKED:
                linked = self.snippet.chunks[chunk.linked]

                src_begin = self.buffer.get_iter_at_mark(linked.begin)
                src_end = self.buffer.get_iter_at_mark(linked.end)
                text = self.buffer.get_text(src_begin, src_end, True)

                dst_begin = self.buffer.get_iter_at_mark(chunk.begin)
                dst_end = self.buffer.get_iter_at_mark(chunk.end)
                self.buffer.delete(dst_begin, dst_end)
                self.buffer.insert(dst_begin, text, -1)
        self.updating_linked_params = False

    def on_view_key_press_event(self, view, event):
        if event.keyval == Gdk.KEY_Tab:
            return self.move_next_param()
        elif event.keyval == Gdk.KEY_ISO_Left_Tab:
            return self.move_prev_param()
        elif event.keyval == Gdk.KEY_Escape:
            self.view.set_state(InsertState())
        return False

    def on_view_draw(self, view, cr):
        for chunk in self.snippet.chunks:
            if chunk.kind not in (Chunk.PLACEHOLDER, Chunk.LINKED):
                continue
            begin = self.buffer.get_iter_at_mark(chunk.begin)
            end = self.buffer.get_iter_at_mark(chunk.end)
            cr.save()
            cr.translate(0.5, 0.5)
            if begin.equal(end):
                self.draw_bar(cr, begin)
            else:
                self.draw_rect(cr, begin, end, dashed=(chunk.kind == Chunk.LINKED))
            cr.restore()

    def draw_bar(self, cr, location):
        rect = self.view.get_iter_location(location)
        rect.x, rect.y = self.view.buffer_to_window_coords(Gtk.TextWindowType.WIDGET, rect.x, rect.y)

        cr.move_to(rect.x, rect.y)
        cr.line_to(rect.x, rect.y + rect.height)
        cr.set_source_rgb(0, 0, 0)
        cr.set_line_width(1)
        cr.stroke()

    def draw_rect(self, cr, begin, end, dashed=False):
        rect = self.view.get_iter_location(begin)
        rect.x, rect.y = self.view.buffer_to_window_coords(Gtk.TextWindowType.WIDGET, rect.x, rect.y)

        rect_end = self.view.get_iter_location(end)
        rect_end.x, rect_end.y = self.view.buffer_to_window_coords(Gtk.TextWindowType.WIDGET, rect_end.x, rect_end.y)
        rect.width = rect_end.x - rect.x

        rect.width -= 1
        rect.height -= 1
        col = self.view.get_style_context().get_color(Gtk.StateFlags.INSENSITIVE)
        col.alpha = 0.7
        Gdk.cairo_set_source_rgba(cr, col)
        cr.set_line_width(1.0)
        if dashed:
            cr.set_dash([2], 0)
        Gdk.cairo_rectangle(cr, rect)
        if dashed:
            cr.stroke()
        else:
            cr.stroke_preserve()
            col.alpha = 0.1
            Gdk.cairo_set_source_rgba(cr, col)
            cr.fill()

class InsertState:
    buffer = None
    handler = None
    view = None

    def initialize(self, view):
        self.view = view
        self.buffer = view.get_buffer()
        self.handler = self.view.connect('key-press-event',
                                         self.on_view_key_press_event)

    def shutdown(self, view):
        if self.handler:
            self.view.disconnect(self.handler)
            self.handler = None

    def on_view_key_press_event(self, view, event):
        if event.keyval == Gdk.KEY_Tab:
            # If the current word is a snippet, switch to the snippet state
            # and insert the snippet.
            current_word = self.buffer.current_word
            snippet = Snippets.get(current_word, None)
            if snippet is not None:
                begin, end = self.buffer.current_word_bounds
                self.buffer.delete(begin, end)
                if (event.state & Gdk.ModifierType.CONTROL_MASK) != 0:
                    self.view.set_state(SuggestState(snippet))
                else:
                    self.view.set_state(SnippetState(snippet()))
                return True
        elif event.keyval == Gdk.KEY_f and (event.state & Gdk.ModifierType.CONTROL_MASK) != 0:
            self.view.set_state(SearchState())

        return False

class SearchState:
    buffer = None
    handler = None
    search = None
    view = None
    tag = None

    def initialize(self, view):
        self.search = ''
        self.view = view
        self.buffer = view.get_buffer()
        self.handler = self.view.connect('key-press-event',
                                         self.on_view_key_press_event)
        self.tag = self.buffer.props.tag_table.lookup('search-tag')
        if not self.tag:
            self.tag = self.buffer.create_tag('search-tag')
            self.tag.props.background = '#fce94f'
            self.tag.props.foreground = '#2e3436'

        self.view.queue_draw()

    def shutdown(self, view):
        if self.handler:
            self.view.disconnect(self.handler)
            self.handler = None
        self.view.queue_draw()
        begin, end = self.buffer.get_bounds()
        self.buffer.remove_tag(self.tag, begin, end)
        highlight.hide()

    def on_view_key_press_event(self, view, event):
        if event.keyval == Gdk.KEY_Escape:
            if highlight.props.visible:
                highlight.hide()
            else:
                self.view.set_state(InsertState())
        elif event.string:
            self.search += event.string
            self.update_search()
        return True

    def update_search(self):
        try:
            regex = re.compile(self.search, re.I)
        except:
            return

        begin, end = self.buffer.get_bounds()
        self.buffer.remove_tag(self.tag, begin, end)

        if not self.search:
            return
        begin, end = self.buffer.get_bounds()
        text = self.buffer.get_text(begin, end, True)

        for match in regex.finditer(text):
            begin, end = match.span()
            begin = self.buffer.get_iter_at_offset(begin)
            end = self.buffer.get_iter_at_offset(end)
            self.buffer.apply_tag(self.tag, begin, end)

        self.view.queue_draw()
        highlight.show()

class SuggestState:
    buffer = None
    handler = None
    snippet = None
    view = None

    def __init__(self, snippet):
        self.snippet = snippet()
        self.snippet_func = snippet

    def initialize(self, view):
        self.view = view
        self.buffer = view.get_buffer()
        self.handler = self.view.connect('key-press-event',
                                         self.on_view_key_press_event)
        self.tag = self.buffer.props.tag_table.lookup('suggest-tag')
        if not self.tag:
            self.tag = self.buffer.create_tag('suggest-tag')
            self.tag.props.style = Pango.Style.ITALIC
            self.tag.props.foreground = '#999999'
        self.insert_snippet()

    def shutdown(self, view):
        if self.handler:
            self.view.disconnect(self.handler)
            self.handler = None

        begin = self.buffer.get_iter_at_mark(self.mb)
        end = self.buffer.get_iter_at_mark(self.eb)
        self.buffer.delete(begin, end)

    def on_view_key_press_event(self, view, event):
        if event.keyval == Gdk.KEY_Escape:
            self.view.set_state(InsertState())
            return True
        elif event.keyval == Gdk.KEY_Return and \
        0 != (event.state & Gdk.ModifierType.CONTROL_MASK):
            self.view.set_state(SnippetState(self.snippet_func()))
            return True
        return False

    def translate_text(self, text):
        if self.view.props.insert_spaces_instead_of_tabs:
            spaces = ' ' * self.view.props.tab_width
            text = text.replace('\t', spaces)
        return text

    def insert_snippet(self):
        location = self.buffer.get_iter_at_mark(self.buffer.get_insert())

        self.mb = self.buffer.create_mark(None, location, True)

        for chunk in self.snippet.chunks:
            if chunk.kind in (Chunk.TYPED_TEXT, Chunk.TEXT):
                text = self.translate_text(chunk.text)
                self.buffer.insert(location, text, -1)
            elif chunk.kind in (Chunk.PLACEHOLDER, Chunk.CURSOR, Chunk.LINKED):
                chunk.begin = location.get_offset()
                self.buffer.insert(location, chunk.text or '')
                chunk.end = location.get_offset()

        self.eb = self.buffer.create_mark(None, location, False)

        begin = self.buffer.get_iter_at_mark(self.mb)
        self.buffer.apply_tag(self.tag, begin, location)

class Buffer(GtkSource.Buffer):
    @property
    def current_word_bounds(self):
        pos = self.get_iter_at_mark(self.get_insert())
        end = pos.copy()
        while pos.backward_char():
            c = pos.get_char()
            if c.isalnum() or c in ('_',):
                continue
            pos.forward_char()
            break
        return pos, end

    @property
    def current_word(self):
        begin, end = self.current_word_bounds
        return self.get_text(begin, end, True)

class View(GtkSource.View):
    state = None

    def __init__(self, tag_table=None, **kwargs):
        GtkSource.View.__init__(self, **kwargs)
        self.props.buffer = Buffer()
        self.set_state(InsertState())

    def set_state(self, state):
        if self.state:
            self.state.shutdown(self)
        self.state = state
        if self.state:
            self.state.initialize(self)

class Chunk(object):
    TYPED_TEXT = 1
    TEXT = 2
    INFORMATIVE = 3
    PLACEHOLDER = 4
    RESULT = 5
    CURSOR = 6
    LINKED = 7

    kind = None
    linked = None
    text = None

    begin = None
    end = None

    def __init__(self, kind, text, linked=None):
        self.kind = kind
        self.text = text
        self.linked = linked

class Snippet(object):
    def __init__(self, chunks):
        self.chunks = chunks

if __name__ == '__main__':
    win = Gtk.Window()
    win.set_title("Editor")
    win.set_default_size(640, 600)
    win.set_position(Gtk.WindowPosition.CENTER)

    overlay = Gtk.Overlay(visible=True)
    win.add(overlay)

    scroller = Gtk.ScrolledWindow()
    overlay.add(scroller)
    scroller.show()

    view = View(show_line_numbers=True,
                show_right_margin=True,
                insert_spaces_instead_of_tabs=True,
                tab_width=3)
    buf = view.get_buffer()
    buf.set_text(file('gb-search-overlay.c').read(), -1)
    begin, _ = buf.get_bounds()
    buf.select_range(begin, begin)
    buf.props.highlight_matching_brackets = False
    scroller.add(view)
    font = Pango.FontDescription.from_string("Monospace 10")
    view.override_font(font)
    view.show()

    c = GtkSource.LanguageManager.get_default().get_language('c')
    buf.set_language(c)

    m = GtkSource.StyleSchemeManager.get_default()
    m.append_search_path('.')
    s = m.get_scheme('solarized-light')
    buf.set_style_scheme(s)

    global highlight
    highlight = Gb.SearchOverlay(widget=view, tag='search-tag', visible=False, opacity=0.75)
    overlay.add_overlay(highlight)

    def build_pr():
        chunks = []
        chunks.append(Chunk(Chunk.TYPED_TEXT, "printf"))
        chunks.append(Chunk(Chunk.TEXT, "(\""))
        chunks.append(Chunk(Chunk.PLACEHOLDER, "format"))
        chunks.append(Chunk(Chunk.TEXT, "\", "))
        chunks.append(Chunk(Chunk.PLACEHOLDER, "param1"))
        chunks.append(Chunk(Chunk.TEXT, ", "))
        chunks.append(Chunk(Chunk.PLACEHOLDER, "param2"))
        chunks.append(Chunk(Chunk.TEXT, ", "))
        chunks.append(Chunk(Chunk.PLACEHOLDER, "param3"))
        chunks.append(Chunk(Chunk.TEXT, ");"))
        return Snippet(chunks)
    Snippets['pr'] = build_pr

    def build_in():
        chunks = []
        chunks.append(Chunk(Chunk.TYPED_TEXT, "#include <"))
        chunks.append(Chunk(Chunk.PLACEHOLDER, "glib.h"))
        chunks.append(Chunk(Chunk.TEXT, ">"))
        return Snippet(chunks)
    Snippets['in'] = build_in

    def build_for():
        chunks = []
        chunks.append(Chunk(Chunk.TYPED_TEXT, "for ("))
        chunks.append(Chunk(Chunk.PLACEHOLDER, "i"))
        chunks.append(Chunk(Chunk.TEXT, " = "))
        chunks.append(Chunk(Chunk.PLACEHOLDER, "0"))
        chunks.append(Chunk(Chunk.TEXT, "; "))
        chunks.append(Chunk(Chunk.LINKED, "i", linked=1))
        chunks.append(Chunk(Chunk.TEXT, " < "))
        chunks.append(Chunk(Chunk.PLACEHOLDER, "count"))
        chunks.append(Chunk(Chunk.TEXT, "; "))
        chunks.append(Chunk(Chunk.LINKED, "i", linked=1))
        chunks.append(Chunk(Chunk.PLACEHOLDER, "++"))
        chunks.append(Chunk(Chunk.TEXT, ") {\n"))
        chunks.append(Chunk(Chunk.TEXT, "\t"))
        chunks.append(Chunk(Chunk.CURSOR, None))
        chunks.append(Chunk(Chunk.TEXT, "\n}"))
        return Snippet(chunks)
    Snippets['for'] = build_for

    def build_fail():
        chunks = []
        chunks.append(Chunk(Chunk.TYPED_TEXT, "g_return_if_fail("))
        chunks.append(Chunk(Chunk.PLACEHOLDER, ""))
        chunks.append(Chunk(Chunk.TEXT, ");"))
        return Snippet(chunks)
    Snippets['fail'] = build_fail

    def build_vfail():
        chunks = []
        chunks.append(Chunk(Chunk.TYPED_TEXT, "g_return_val_if_fail("))
        chunks.append(Chunk(Chunk.PLACEHOLDER, ""))
        chunks.append(Chunk(Chunk.TEXT, ", "))
        chunks.append(Chunk(Chunk.PLACEHOLDER, "NULL"))
        chunks.append(Chunk(Chunk.TEXT, ");"))
        return Snippet(chunks)
    Snippets['vfail'] = build_vfail

    win.connect("delete-event", lambda *_: Gtk.main_quit())
    #win.set_visual(Gdk.Screen.get_default().get_rgba_visual())
    win.present()
    Gtk.main()
