/* gb-search-overlay.h
 *
 * Copyright (C) 2013 Christian Hergert <christian@hergert.me>
 *
 * This file is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef GB_SEARCH_OVERLAY_H
#define GB_SEARCH_OVERLAY_H

#include <gtk/gtk.h>

G_BEGIN_DECLS

#define GB_TYPE_SEARCH_OVERLAY            (gb_search_overlay_get_type())
#define GB_SEARCH_OVERLAY(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj), GB_TYPE_SEARCH_OVERLAY, GbSearchOverlay))
#define GB_SEARCH_OVERLAY_CONST(obj)      (G_TYPE_CHECK_INSTANCE_CAST ((obj), GB_TYPE_SEARCH_OVERLAY, GbSearchOverlay const))
#define GB_SEARCH_OVERLAY_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass),  GB_TYPE_SEARCH_OVERLAY, GbSearchOverlayClass))
#define GB_IS_SEARCH_OVERLAY(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), GB_TYPE_SEARCH_OVERLAY))
#define GB_IS_SEARCH_OVERLAY_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass),  GB_TYPE_SEARCH_OVERLAY))
#define GB_SEARCH_OVERLAY_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS ((obj),  GB_TYPE_SEARCH_OVERLAY, GbSearchOverlayClass))

typedef struct _GbSearchOverlay        GbSearchOverlay;
typedef struct _GbSearchOverlayClass   GbSearchOverlayClass;
typedef struct _GbSearchOverlayPrivate GbSearchOverlayPrivate;

struct _GbSearchOverlay
{
   GtkDrawingArea parent;

   /*< private >*/
   GbSearchOverlayPrivate *priv;
};

struct _GbSearchOverlayClass
{
   GtkDrawingAreaClass parent_class;
};

GType      gb_search_overlay_get_type (void) G_GNUC_CONST;
GtkWidget *gb_search_overlay_new      (void);

G_END_DECLS

#endif /* GB_SEARCH_OVERLAY_H */
