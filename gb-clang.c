/* gb-clang.c
 *
 * Copyright (C) 2013 Christian Hergert <christian@hergert.me>
 *
 * This file is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <clang-c/Index.h>
#include <gtksourceview/gtksourcestylescheme.h>
#include <gtksourceview/gtksourcebuffer.h>
#include <glib/gi18n.h>

#include "gb-clang.h"

G_DEFINE_TYPE(GbClang, gb_clang, G_TYPE_OBJECT)

#define QUEUE_SENTINAL ((gpointer)&gClangSentinal)

struct _GbClangPrivate
{
   GThread     *thread;
   GAsyncQueue *queue;
   gboolean     translating;
   CXIndex      index;
   GHashTable  *args;
   GHashTable  *buffers;
   GHashTable  *units;
};

enum
{
   PROP_0,
   LAST_PROP
};

//static GParamSpec *gParamSpecs[LAST_PROP];
static gchar gClangSentinal;

GbClang *
gb_clang_new (void)
{
   return g_object_new(GB_TYPE_CLANG, NULL);
}

static CXSourceRange
get_buffer_range (CXTranslationUnit  unit,
                  CXFile             file,
                  GtkTextBuffer     *buffer)
{
   CXSourceLocation bloc;
   CXSourceLocation eloc;
   GtkTextIter begin;
   GtkTextIter end;
   guint offset;

   gtk_text_buffer_get_bounds(buffer, &begin, &end);
   offset = gtk_text_iter_get_offset(&end);
   bloc = clang_getLocationForOffset(unit, file, 0);
   eloc = clang_getLocationForOffset(unit, file, offset);
   return clang_getRange(bloc, eloc);
}

static gboolean
compare_file (CXFile       file,
              const gchar *filename)
{
   const char *str;
   CXString xstr;
   gboolean ret;

   xstr = clang_getFileName(file);
   str = clang_getCString(xstr);
   ret = !g_strcmp0(str, filename);
   clang_disposeString(xstr);

   return ret;
}

static CXDiagnostic
gb_clang_get_diagnostic (GbClang       *clang,
                         const gchar   *filename,
                         GtkTextBuffer *buffer,
                         GtkTextIter   *begin,
                         GtkTextIter   *end)
{
   CXTranslationUnit unit;
   CXSourceLocation loc;
   GbClangPrivate *priv;
   CXDiagnostic diag;
   unsigned offset;
   CXFile file;
   guint bo;
   guint eo;
   guint count;
   guint i;

   g_return_val_if_fail(GB_IS_CLANG(clang), NULL);
   g_return_val_if_fail(filename, NULL);
   g_return_val_if_fail(GTK_IS_TEXT_BUFFER(buffer), NULL);
   g_return_val_if_fail(begin, NULL);
   g_return_val_if_fail(end, NULL);

   priv = clang->priv;

   /*
    * TODO: Make this really fast. Probably involves keeping track of
    *       the diagnostics in an array that can be binary searched.
    *       On every translation, we would replace that array.
    */

   if (!(unit = g_hash_table_lookup(priv->units, filename))) {
      return FALSE;
   }

   bo = gtk_text_iter_get_offset(begin);
   eo = gtk_text_iter_get_offset(end);

   count = clang_getNumDiagnostics(unit);
   for (i = 0; i < count; i++) {
      diag = clang_getDiagnostic(unit, i);
      loc = clang_getDiagnosticLocation(diag);
      clang_getExpansionLocation(loc, &file, NULL, NULL, &offset);
      if (compare_file(file, filename) && (offset >= bo) && (offset <= eo)) {
         return diag;
      }
      clang_disposeDiagnostic(diag);
   }

   return NULL;
}

gboolean
gb_clang_get_has_diagnostic (GbClang       *clang,
                             const gchar   *filename,
                             GtkTextBuffer *buffer,
                             GtkTextIter   *begin,
                             GtkTextIter   *end)
{
   CXDiagnostic diag;

   diag = gb_clang_get_diagnostic(clang, filename, buffer, begin, end);
   clang_disposeDiagnostic(diag);
   return !!diag;
}

void
gb_clang_highlight (GbClang       *clang,
                    const gchar   *filename,
                    GtkTextBuffer *buffer)
{
   CXTranslationUnit unit;
   GtkTextTagTable *tag_table;
   GbClangPrivate *priv;
   CXSourceRange range;
   GtkTextTag *tag;
   GtkTextIter begin;
   GtkTextIter end;
   unsigned offset;
   unsigned n_tokens;
   unsigned i;
   CXToken *tokens;
   CXFile file;

   g_return_if_fail(GB_IS_CLANG(clang));
   g_return_if_fail(filename);
   g_return_if_fail(GTK_IS_TEXT_BUFFER(buffer));

   priv = clang->priv;

   if (!(unit = g_hash_table_lookup(priv->units, filename))) {
      g_print("No matching unit.\n");
      return;
   }

   tag_table = gtk_text_buffer_get_tag_table(buffer);
   tag = gtk_text_tag_table_lookup(tag_table, "clang-type");

   gtk_text_buffer_get_bounds(buffer, &begin, &end);
   gtk_text_buffer_remove_tag(buffer, tag, &begin, &end);

   file = clang_getFile(unit, filename);
   range = get_buffer_range(unit, file, buffer);

   clang_tokenize(unit, range, &tokens, &n_tokens);

   for (i = 0; i < n_tokens; i++) {
      if (clang_getTokenKind(tokens[i]) == CXToken_Identifier) {
         CXSourceLocation loc;
         CXSourceLocation endloc;
         CXCursor cursor;

         loc = clang_getTokenLocation(unit, tokens[i]);
         cursor = clang_getCursor(unit, loc);

         switch (clang_getCursorKind(cursor)) {
         case CXCursor_TypeRef:
            range = clang_getCursorExtent(cursor);
            loc = clang_getRangeStart(range);
            clang_getExpansionLocation(loc, NULL, NULL, NULL, &offset);
            gtk_text_buffer_get_iter_at_offset(buffer, &begin, offset);
            endloc = clang_getRangeEnd(range);
            clang_getExpansionLocation(endloc, NULL, NULL, NULL, &offset);
            gtk_text_buffer_get_iter_at_offset(buffer, &end, offset);
            gtk_text_buffer_apply_tag(buffer, tag, &begin, &end);
            break;
         default:
            break;
         }

#if 0
         CXString xstr;
         const gchar *str;

         xstr = clang_getTokenSpelling(unit, tokens[i]);
         str = clang_getCString(xstr);
         g_print("%s\n", str);
         clang_disposeString(xstr);
#endif
      }
   }

   clang_disposeTokens(unit, tokens, n_tokens);
}

static gboolean
translate_highlight (gpointer data)
{
   gpointer *args = data;

   gb_clang_translate(args[0], args[1], args[2]);
   gb_clang_highlight(args[0], args[1], args[2]);

   g_object_unref(args[0]);
   g_free(args[1]);
   g_object_unref(args[2]);

   g_free(args);

   return FALSE;
}

void
gb_clang_add_buffer (GbClang        *clang,
                     const gchar    *filename,
                     GtkTextBuffer  *buffer,
                     gchar         **args)
{
   GtkSourceStyleScheme *scheme;
   GtkSourceStyle *style;
   GbClangPrivate *priv;
   GtkTextTag *type_tag;

   g_return_if_fail(GB_IS_CLANG(clang));
   g_return_if_fail(GTK_IS_TEXT_BUFFER(buffer));
   g_return_if_fail(filename);

   priv = clang->priv;

   g_hash_table_replace(priv->buffers,
                        g_object_ref(buffer),
                        g_strdup(filename));

   g_hash_table_replace(priv->args, g_strdup(filename), g_strdupv(args));

   gtk_text_buffer_create_tag(buffer, "clang-diagnostic",
                              //"background", "#cc0000",
                              //"foreground", "#d3d7cf",
                              "underline", PANGO_UNDERLINE_ERROR,
                              NULL);

   type_tag = gtk_text_buffer_create_tag(buffer, "clang-type",
                                         "foreground", "#0000FF",
                                         NULL);

   scheme = gtk_source_buffer_get_style_scheme(GTK_SOURCE_BUFFER(buffer));
   style = gtk_source_style_scheme_get_style(scheme, "def:type");
   g_object_bind_property(style, "foreground", type_tag, "foreground", G_BINDING_SYNC_CREATE);
   g_object_bind_property(style, "background", type_tag, "background", G_BINDING_SYNC_CREATE);

   /*
    * When this work is all done async, this wont matter so much anymore.
    * But for now, deferring until after the app gets started and into its
    * main loop.
    */
   {
      gpointer *args = g_new0(gpointer, 3);
      args[0] = g_object_ref(clang);
      args[1] = g_strdup(filename);
      args[2] = g_object_ref(buffer);
      g_timeout_add(150, translate_highlight, args);
   }
}

static void
gb_clang_update_diagnostic (GbClang           *clang,
                            const gchar       *path,
                            GtkTextBuffer     *buffer,
                            CXTranslationUnit  unit,
                            CXDiagnostic       diag,
                            GtkTextTag        *tag)
{
   CXSourceLocation loc;
   GtkTextIter begin;
   GtkTextIter iter;
   unsigned offset;
   unsigned count;

   loc = clang_getDiagnosticLocation(diag);
   clang_getExpansionLocation(loc, NULL, NULL, NULL, &offset);

   gtk_text_buffer_get_iter_at_offset(buffer, &iter, offset);
   gtk_text_iter_assign(&begin, &iter);

   count = clang_getDiagnosticNumRanges(diag);
   if (!count) {
      gtk_text_iter_forward_to_line_end(&iter);
      gtk_text_buffer_apply_tag(buffer, tag, &begin, &iter);
   } else {
      /*
       * TODO: Highlight ranges.
       */
   }
}

static void
gb_clang_update_diagnostics (GbClang           *clang,
                             const gchar       *path,
                             GtkTextBuffer     *buffer,
                             CXTranslationUnit  unit)
{
   CXSourceLocation loc;
   GtkTextTagTable *tag_table;
   CXDiagnostic diag;
   GtkTextIter begin;
   GtkTextIter end;
   GtkTextTag *tag;
   unsigned count;
   unsigned i;
   CXFile file;

   g_return_if_fail(GB_IS_CLANG(clang));
   g_return_if_fail(path);
   g_return_if_fail(GTK_IS_TEXT_BUFFER(buffer));
   g_return_if_fail(unit);

   tag_table = gtk_text_buffer_get_tag_table(buffer);
   tag = gtk_text_tag_table_lookup(tag_table, "clang-diagnostic");
   gtk_text_buffer_get_bounds(buffer, &begin, &end);
   gtk_text_buffer_remove_tag(buffer, tag, &begin, &end);

   count = clang_getNumDiagnostics(unit);
   for (i = 0; i < count; i++) {
      diag = clang_getDiagnostic(unit, i);
      loc = clang_getDiagnosticLocation(diag);
      clang_getExpansionLocation(loc, &file, NULL, NULL, NULL);
      if (compare_file(file, path)) {
         gb_clang_update_diagnostic(clang, path, buffer, unit, diag, tag);
      }
      clang_disposeDiagnostic(diag);
   }
}

void
gb_clang_set_args (GbClang      *clang,
                   const gchar  *path,
                   gchar       **argv)
{
   g_return_if_fail(GB_IS_CLANG(clang));
   g_return_if_fail(path);
   g_hash_table_replace(clang->priv->args, g_strdup(path), g_strdupv(argv));
}

static void
gb_clang_do_translate (GbClang       *clang,
                       const gchar   *path,
                       GtkTextBuffer *buffer)
{
}

void
gb_clang_translate (GbClang       *clang,
                    const gchar   *path,
                    GtkTextBuffer *buffer)
{
   struct CXUnsavedFile unsaved_files;
   const char * const *argv;
   CXTranslationUnit unit;
   GbClangPrivate *priv;
   GtkTextIter begin;
   GtkTextIter end;
   //unsigned options;
   unsigned n_unsaved_files = 1;
   unsigned argc;
   int r;

   g_return_if_fail(GB_IS_CLANG(clang));
   g_return_if_fail(path);

   priv = clang->priv;

   gtk_text_buffer_get_bounds(buffer, &begin, &end);
   memset(&unsaved_files, 0, sizeof unsaved_files);
   unsaved_files.Filename = path;
   unsaved_files.Contents = gtk_text_buffer_get_text(buffer, &begin, &end, FALSE);
   unsaved_files.Length = strlen(unsaved_files.Contents);

   /*
    * TODO: Eventually this needs to be done in a thread.
    */

   if ((unit = g_hash_table_lookup(priv->units, path))) {
      r = clang_reparseTranslationUnit(unit, n_unsaved_files, &unsaved_files,
                                       clang_defaultReparseOptions(unit));
      if (r == 0) {
         gb_clang_update_diagnostics(clang, path, buffer, unit);
      } else {
         g_hash_table_remove(priv->units, path);
      }
      goto cleanup;

      //g_hash_table_remove(priv->units, path);
   }

   argv = g_hash_table_lookup(priv->args, path);
   argc = argv ? g_strv_length((gchar **)argv) : 0;

   unit =
      clang_createTranslationUnitFromSourceFile(priv->index,
                                                path,
                                                argc,
                                                argv,
                                                n_unsaved_files,
                                                &unsaved_files);
   if (!unit) {
      /*
       * TODO: Handle compilation failure.
       */
   } else {
      g_hash_table_insert(priv->units, g_strdup(path), unit);
      gb_clang_update_diagnostics(clang, path, buffer, unit);
   }

#if 0
   } else {
      options = clang_defaultReparseOptions(unit);
      if (clang_reparseTranslationUnit(unit,
                                       n_unsaved_files,
                                       &unsaved_files,
                                       options) != 0) {
         g_hash_table_remove(priv->units, path);
      }
   }
#endif

cleanup:
   g_free((gchar *)unsaved_files.Contents);
}

gchar *
gb_clang_get_tooltip (GbClang       *clang,
                      const gchar   *filename,
                      GtkTextBuffer *buffer,
                      GtkTextIter   *begin,
                      GtkTextIter   *end)
{
   CXTranslationUnit unit;
   GbClangPrivate *priv;
   CXDiagnostic diag;
   CXString xstr;
   unsigned options;
   CXFile file;
   gchar *str;

   g_return_val_if_fail(GB_IS_CLANG(clang), NULL);
   g_return_val_if_fail(filename, NULL);

   priv = clang->priv;

   if (!(unit = g_hash_table_lookup(priv->units, filename))) {
      return NULL;
   }

   if (!(file = clang_getFile(unit, filename))) {
      return NULL;
   }

   if (!(diag = gb_clang_get_diagnostic(clang, filename, buffer, begin, end))) {
      return NULL;
   }

   options = clang_defaultDiagnosticDisplayOptions();
   xstr = clang_formatDiagnostic(diag, options);
   str = g_strdup(clang_getCString(xstr));

   clang_disposeString(xstr);
   clang_disposeDiagnostic(diag);

   return str;
}

static gpointer
gb_clang_worker (gpointer data)
{
   GbClang *clang = data;
   struct {
      gchar         *path;
      GtkTextBuffer *buffer;
   } *job;

   while (QUEUE_SENTINAL != (job = g_async_queue_pop(clang->priv->queue))) {
      gb_clang_do_translate(clang, job->path, job->buffer);
      g_free(job->path);
      g_object_unref(job->buffer);
      g_free(job);
   }

   return NULL;
}

static void
gb_clang_finalize (GObject *object)
{
   GbClangPrivate *priv;

   priv = GB_CLANG(object)->priv;

   g_async_queue_push(priv->queue, QUEUE_SENTINAL);
   g_thread_join(priv->thread);
   priv->thread = NULL;

   g_clear_pointer(&priv->index, clang_disposeIndex);
   g_clear_pointer(&priv->args, g_hash_table_unref);
   g_clear_pointer(&priv->buffers, g_hash_table_unref);
   g_clear_pointer(&priv->units, g_hash_table_unref);
   g_clear_pointer(&priv->queue, g_async_queue_unref);

   G_OBJECT_CLASS(gb_clang_parent_class)->finalize(object);
}

static void
gb_clang_get_property (GObject    *object,
                       guint       prop_id,
                       GValue     *value,
                       GParamSpec *pspec)
{
   //GbClang *clang = GB_CLANG(object);

   switch (prop_id) {
   default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID(object, prop_id, pspec);
   }
}

static void
gb_clang_set_property (GObject      *object,
                       guint         prop_id,
                       const GValue *value,
                       GParamSpec   *pspec)
{
   //GbClang *clang = GB_CLANG(object);

   switch (prop_id) {
   default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID(object, prop_id, pspec);
   }
}

static void
gb_clang_class_init (GbClangClass *klass)
{
   GObjectClass *object_class;

   object_class = G_OBJECT_CLASS(klass);
   object_class->finalize = gb_clang_finalize;
   object_class->get_property = gb_clang_get_property;
   object_class->set_property = gb_clang_set_property;
   g_type_class_add_private(object_class, sizeof(GbClangPrivate));
}

static void
gb_clang_init (GbClang *clang)
{
   clang->priv = G_TYPE_INSTANCE_GET_PRIVATE(clang,
                                             GB_TYPE_CLANG,
                                             GbClangPrivate);

   clang->priv->index = clang_createIndex(0, 0);

   clang->priv->buffers =
      g_hash_table_new_full(g_direct_hash,
                            g_direct_equal,
                            g_object_unref,
                            g_free);

   clang->priv->units =
      g_hash_table_new_full(g_str_hash,
                            g_str_equal,
                            g_free,
                            g_free);

   clang->priv->args =
      g_hash_table_new_full(g_str_hash,
                            g_str_equal,
                            g_free,
                            (GDestroyNotify)g_strfreev);

   clang->priv->queue = g_async_queue_new();
   clang->priv->thread = g_thread_new("gb-clang-worker",
                                      gb_clang_worker,
                                      clang);

   clang_CXIndex_setGlobalOptions(clang->priv->index,
                                  CXGlobalOpt_ThreadBackgroundPriorityForEditing);
}
