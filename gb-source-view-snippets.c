/* gb-source-view-snippets.c
 *
 * Copyright (C) 2013 Christian Hergert <christian@hergert.me>
 *
 * This file is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <glib/gi18n.h>

#include "gb-source-view-snippets.h"

G_DEFINE_TYPE(GbSourceViewSnippets, gb_source_view_snippets, G_TYPE_OBJECT)

struct _GbSourceViewSnippetsPrivate
{
   GHashTable *snippets;
};

void
gb_source_view_snippets_add (GbSourceViewSnippets *snippets,
                             GbSourceViewSnippet  *snippet)
{
   const gchar *key;

   g_return_if_fail(GB_IS_SOURCE_VIEW_SNIPPETS(snippets));
   g_return_if_fail(GB_IS_SOURCE_VIEW_SNIPPET(snippet));

   key = gb_source_view_snippet_get_key(snippet);
   g_hash_table_replace(snippets->priv->snippets,
                        g_strdup(key),
                        g_object_ref(snippet));
}

GbSourceViewSnippet *
gb_source_view_snippets_lookup (GbSourceViewSnippets *snippets,
                                const gchar          *key)
{
   GbSourceViewSnippet *snippet;

   g_return_val_if_fail(GB_IS_SOURCE_VIEW_SNIPPETS(snippets), NULL);
   g_return_val_if_fail(key, NULL);

   if ((snippet = g_hash_table_lookup(snippets->priv->snippets, key))) {
      return gb_source_view_snippet_copy(snippet);
   }

   return NULL;
}

void
gb_source_view_snippets_remove (GbSourceViewSnippets *snippets,
                                GbSourceViewSnippet  *snippet)
{
   const gchar *key;

   g_return_if_fail(GB_IS_SOURCE_VIEW_SNIPPETS(snippets));
   g_return_if_fail(GB_IS_SOURCE_VIEW_SNIPPET(snippet));

   key = gb_source_view_snippet_get_key(snippet);
   g_hash_table_remove(snippets->priv->snippets, key);
}

static void
gb_source_view_snippets_finalize (GObject *object)
{
   GbSourceViewSnippetsPrivate *priv;

   priv = GB_SOURCE_VIEW_SNIPPETS(object)->priv;

   g_clear_pointer(&priv->snippets, g_hash_table_unref);

   G_OBJECT_CLASS(gb_source_view_snippets_parent_class)->finalize(object);
}

static void
gb_source_view_snippets_class_init (GbSourceViewSnippetsClass *klass)
{
   GObjectClass *object_class;

   object_class = G_OBJECT_CLASS(klass);
   object_class->finalize = gb_source_view_snippets_finalize;
   g_type_class_add_private(object_class, sizeof(GbSourceViewSnippetsPrivate));
}

static void
gb_source_view_snippets_init (GbSourceViewSnippets *snippets)
{
   snippets->priv =
      G_TYPE_INSTANCE_GET_PRIVATE(snippets,
                                  GB_TYPE_SOURCE_VIEW_SNIPPETS,
                                  GbSourceViewSnippetsPrivate);
   snippets->priv->snippets =
      g_hash_table_new_full(g_str_hash, g_str_equal, g_free, g_object_unref);
}
