all: editor

WARNINGS :=
WARNINGS += -Wall
#WARNINGS += -Werror

PKGS :=
PKGS += gtk+-3.0
PKGS += gtksourceview-3.0

OBJECTS :=
OBJECTS += gb-change-tracker.o
OBJECTS += gb-clang.o
OBJECTS += gb-search-overlay.o
OBJECTS += gb-source-gutter-renderer-changes.o
OBJECTS += gb-source-gutter-renderer-clang.o
OBJECTS += gb-source-view.o
OBJECTS += gb-source-view-snippet.o
OBJECTS += gb-source-view-snippet-chunk.o
OBJECTS += gb-source-view-snippets.o
OBJECTS += gb-source-view-state.o
OBJECTS += gb-source-view-state-insert.o
OBJECTS += gb-source-view-state-snippet.o

LIBS :=
LIBS += -L/usr/lib64/llvm
LIBS += -lclang

%.o: %.c %.h
	$(CC) -g -fPIC -o $@ $(WARNINGS) -c $(shell pkg-config --cflags $(PKGS)) $*.c

editor: $(OBJECTS) main.c
	$(CC) -g -o $@ $(WARNINGS) $(shell pkg-config --cflags --libs $(PKGS)) $(OBJECTS) main.c $(LIBS)

clean:
	rm -f *.o editor
