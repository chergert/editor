/* gb-source-view-snippet.h
 *
 * Copyright (C) 2013 Christian Hergert <christian@hergert.me>
 *
 * This file is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef GB_SOURCE_VIEW_SNIPPET_H
#define GB_SOURCE_VIEW_SNIPPET_H

#include <gtk/gtk.h>

#include "gb-source-view-snippet-chunk.h"

G_BEGIN_DECLS

#define GB_TYPE_SOURCE_VIEW_SNIPPET            (gb_source_view_snippet_get_type())
#define GB_SOURCE_VIEW_SNIPPET(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj), GB_TYPE_SOURCE_VIEW_SNIPPET, GbSourceViewSnippet))
#define GB_SOURCE_VIEW_SNIPPET_CONST(obj)      (G_TYPE_CHECK_INSTANCE_CAST ((obj), GB_TYPE_SOURCE_VIEW_SNIPPET, GbSourceViewSnippet const))
#define GB_SOURCE_VIEW_SNIPPET_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass),  GB_TYPE_SOURCE_VIEW_SNIPPET, GbSourceViewSnippetClass))
#define GB_IS_SOURCE_VIEW_SNIPPET(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), GB_TYPE_SOURCE_VIEW_SNIPPET))
#define GB_IS_SOURCE_VIEW_SNIPPET_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass),  GB_TYPE_SOURCE_VIEW_SNIPPET))
#define GB_SOURCE_VIEW_SNIPPET_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS ((obj),  GB_TYPE_SOURCE_VIEW_SNIPPET, GbSourceViewSnippetClass))

typedef struct _GbSourceViewSnippet        GbSourceViewSnippet;
typedef struct _GbSourceViewSnippetClass   GbSourceViewSnippetClass;
typedef struct _GbSourceViewSnippetPrivate GbSourceViewSnippetPrivate;

struct _GbSourceViewSnippet
{
   GObject parent;

   /*< private >*/
   GbSourceViewSnippetPrivate *priv;
};

struct _GbSourceViewSnippetClass
{
   GObjectClass parent_class;
};

void                 gb_source_view_snippet_add_chunk      (GbSourceViewSnippet      *snippet,
                                                            GbSourceViewSnippetChunk *chunk);
void                 gb_source_view_snippet_draw           (GbSourceViewSnippet      *snippet,
                                                            GtkWidget                *widget,
                                                            cairo_t                  *cr);
void                 gb_source_view_snippet_finish         (GbSourceViewSnippet      *snippet);
const gchar         *gb_source_view_snippet_get_key        (GbSourceViewSnippet      *snippet);
GtkTextMark         *gb_source_view_snippet_get_mark_begin (GbSourceViewSnippet      *snippet);
GtkTextMark         *gb_source_view_snippet_get_mark_end   (GbSourceViewSnippet      *snippet);
GType                gb_source_view_snippet_get_type       (void) G_GNUC_CONST;
void                 gb_source_view_snippet_insert         (GbSourceViewSnippet      *snippet,
                                                            GtkTextBuffer            *buffer,
                                                            GtkTextIter              *location);
gboolean             gb_source_view_snippet_move_next      (GbSourceViewSnippet      *snippet);
gboolean             gb_source_view_snippet_move_previous  (GbSourceViewSnippet      *snippet);
void                 gb_source_view_snippet_remove         (GbSourceViewSnippet      *snippet);
GbSourceViewSnippet *gb_source_view_snippet_copy           (GbSourceViewSnippet      *snippet);
void                 gb_source_view_snippet_insert_text    (GbSourceViewSnippet      *snippet,
                                                            GtkTextBuffer            *buffer,
                                                            GtkTextIter              *location,
                                                            const gchar              *text,
                                                            guint                     length);
void                 gb_source_view_snippet_delete_range   (GbSourceViewSnippet      *snippet,
                                                            GtkTextBuffer            *buffer,
                                                            GtkTextIter              *begin,
                                                            GtkTextIter              *end);

G_END_DECLS

#endif /* GB_SOURCE_VIEW_SNIPPET_H */
